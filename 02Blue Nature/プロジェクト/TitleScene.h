/* GE2A09 菅野陽暉	 最終変更日：2020/10/07 */
/* タイトル表示、スタートラベルの点滅	    */
#pragma once

//インクルード
#include "Engine/GameObject.h"

//タイトルシーンを管理するクラス
class TitleScene : public GameObject
{
private:
	const float TITLE_SIZE;		//タイトルロゴのサイズ
	const float TITLE_POS;		//タイトルロゴの位置
	const float START_SIZE;		//スタートラベルのサイズ
	const float START_POS;		//スタートラベルの位置

	int titleHandle_;		//タイトルロゴの画像番号
	int startHandle_;		//スタートラベルの画像番号
	float imageAlpha_;		//画像の透明度
	float alphaIncDec_;		//アルファ値の増減値
	GameObject* player_;	//プレイヤー情報

	int credit_;			//クレジットモデル
	int creditPict_;		//クレジット画像
	Transform creditPos_;	//クレジット位置
	Transform pictPos_;		//クレジット表示位置

public:
	//引数：GameObject*（親オブジェクト）
	//返値：なし
	//コンストラクタ
	TitleScene(GameObject* parent);

	//引数：なし
	//返値：void
	//初期化
	void Initialize() override;

	//引数：なし
	//返値：void
	//更新
	void Update() override;

	//引数：なし
	//返値：void
	//描画
	void Draw() override;

	//引数：なし
	//返値：void
	//開放
	void Release() override;

	//引数：GameObject*（プレイヤー）
	//返値：void
	//プレイヤー情報の設定
	void SetPlayer(GameObject* player);
};