#pragma once

#include <string>
#include <array>
#include <memory>
#include "Engine/GameObject.h"

class SceneManager;

//スプラッシュシーンを管理するクラス
class SplashScene : public GameObject
{
	std::array<int, 2> hPicts_;			//画像番号
	std::array<int, 2>::iterator itr_;	//hPicts_イテレータ
	const std::string RESOURCE_PATH_;	//検索対象パス
	float elapsedTime_;					//経過時間
	const float LOGO_TRANSITION_TIME_;			//ロゴを切り替える時間
	const float SPLASH_FINISH_TIME_;			//シーンが終了する時間

	const float FADE_OUT_RATIO_;		//ロゴが完全にフェードアウトする時間の比率を調整可能	
	const float FADE_IN_SMOOTHNESS_;	//ロゴがフェードインする時の滑らかさを調節可能
	const float START_POS_SHIFT_;		//ロゴの表示開始位置を調整

	const int UV_X_;					//切り抜きたい範囲のx座標
	const int UV_Y_;					//切り抜きたい範囲のx座標
	const int CUT_WIDTH_;				//切り抜きたい範囲の幅
	const int CUT_HEIGHT_;			//切り抜きたい範囲の高さ

	SceneManager* pSceneManager_;	//SceneManagerポインタ

	int audioBuble_;	//サウンド
public:

	//コンストラクタ
	//params : GameObject* parent
	//return : NULL
	SplashScene(GameObject* parent);

	//初期化
	//params : NULL
	//return : NULL
	void Initialize() override;

	//更新
	//params : NULL
	//return : NULL
	void Update() override;

	//描画
	//params : NULL
	//return : NULL
	void Draw() override;

	//開放
	//params : NULL
	//return : NULL
	void Release() override;
};