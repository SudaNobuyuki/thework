#pragma once
/*
*
* プレイヤーおよびプレイヤーの動作にかかわる処理を行うクラス
* 作者名：須田信行
* 制作日：2020/09/09〜
* その他記述すべきことがある場合の記述
* 水の抵抗・加速度は未実装予定
* テストデータとしてバック(後進:Sキー)実装
*
*/

//インクルード
#include "Engine/GameObject.h"


//ディファイン
#define ROTATE_X XMConvertToRadians(transform_.rotate_.vecX)	//このゲームオブジェクトに用意されたtransform_の回転のX成分(ラジアン度)
#define ROTATE_Y XMConvertToRadians(transform_.rotate_.vecY) 	//このゲームオブジェクトに用意されたtransform_の回転のY成分(ラジアン度)
#define ROTATE_Z XMConvertToRadians(transform_.rotate_.vecZ) 	//このゲームオブジェクトに用意されたtransform_の回転のZ成分(ラジアン度)
#define OVER_RECTANGULAR	450.0f								//地面との当たり判定において角度調整用の数値
#define UNDER_RECTANGULAR	90.0f								//地面との当たり判定において角度調整用の数値

//プレイヤーを管理するクラス
class Player : public GameObject
{
private:

	XMVECTOR collCenter_;
	XMVECTOR collSize_;

	//プレイヤーの動作(前進)に関するメンバ
	float moveStrength_;		//移動量の強さ(最大値)

	//プレイヤーの動作(回転)に関するメンバ
	float rotateVolume_;		//1F当たりのY軸回転量
	float mouseSensitivity_;	//マウス感度

	//プレイヤーのX軸傾きに関するメンバ
	float maxAngle_;			//X軸角度の可変領域最大値
	float moveAngleSpd_;		//上下へ向きを変更させるスピード

	//回転に関するメンバ
	XMMATRIX rotateMatX_;		//ベクトルをプレイヤーの向き(X)によって回転する際に使用
	XMMATRIX rotateMatY_;		//ベクトルをプレイヤーの向き(Y)によって回転する際に使用

	//移動に関するフラグ
	char hitDownFlag_;			//正面のレイの状態によって更新
	char hitStreatFlag_;		//真下のレイの状態によって更新

	//回転に関するフラグ
	bool disGroundFlag_;		//地面との距離で更新
	float fitGroundAngle_;		//地面に当たったときの角度によってその地面に沿わせるまでの角度の値

	//地面に触れているときのプレイヤーの回転に関するメンバ
	float diffAngle_;			//調整したい角度
	float splitAngle_;			//求めた角度を分割する
	float rotateMax_;			//一回で調整できる最大角度
	float rotateMin_;			//一回で調整できる最小角度

	//移動制限に関するメンバ
	float upRest_;				//上方向の制限
	float rightRest_;			//右方向の制限
	float leftRest_;			//左方向の制限
	float streatRest_;			//正面方向の制限
	float backRest_;			//後ろ方向の制限
	char restFlag_;				//制限に関するフラグ
	float adMove_;				//当たったときの位置調整の値

	//12/17
	//伊達追記
	//レイキャストにより当たり判定を行うモデルハンドルに関するメンバ
	struct RayCastModel
	{
		int hGroundModel;	//当たり判定を行う地面のモデルハンドル
		int hMorayEel;		//当たり判定を行うウツボのモデルハンドル
		int hWorldGround;	//当たり判定を行う世界の地面のモデルハンドル（全世界を覆うモデル）
							//コンストラクタ
		RayCastModel() :hGroundModel(-1), hMorayEel(-1), hWorldGround(-1) {}
	}hRayCastModel_;



	//引数:なし
	//戻値:なし
	//プレイヤーの移動に関する動作をブロック化した関数
	void BlockingPlayerMove();

	//引数:入力を確認できた際に 1を返すキーコード
	//引数:入力を確認できた際に-1を返すキーコード
	//戻値:上記通り、ただしともにtrueであったり、ともにfalseの場合には0を返す
	//キー入力チェック関数
	int CheckInput(int keyIDPlus, int keyIDMinus);

	//引数:なし
	//戻値:なし
	//マウス操作なのかキーボード操作なのかをブロック化した関数
	void BlockingRotate();

	//引数:なし
	//戻値:なし
	//マウス操作時の方向転換をブロック化した関数
	void BlockingMouseRotate();

	//引数:なし
	//戻値:なし
	//プレイヤーの回転に関する動作をブロック化した関数
	void BlockingPlayerRotate();

	//引数:なし
	//戻値:なし
	//プレイヤーを上・下に向かせる(遊泳)動作をブロック化した関数
	void BlockingPlayerSwim();

	//引数:なし
	//戻値:なし
	//地面との当たり判定処理
	void RayShot();

	void ExecutionRayCast(bool* flg, int hRayCastModel);


	//引数:プレイヤーの移動量
	//戻値:なし
	//移動できる範囲での移動処理を行う関数
	void MoveRest(XMVECTOR moveVec);


public:
	//コンストラクタ
	Player(GameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//引数:なし
	//戻値:回転行列
	//プレイヤーの回転行列
	XMMATRIX GetRotMatX() { return rotateMatX_; }
	XMMATRIX GetRotMatY() { return rotateMatY_; }

	/************
	　マウス感度
	 *************/

	 //引数:設定マウス感度
	 //戻値:なし
	 //マウス感度設定
	void SetMouseSensitivity(float mouseSensitivity) { mouseSensitivity_ = mouseSensitivity; };

	//引数:なし
	//戻値:現在のマウス感度
	//マウス感度取得
	float GetMouseSensitivity() { return mouseSensitivity_; };

	//伊達追記
	//追記理由：地面のモデルは複数存在する、その中で、すべてと当たり判定を行うわけにはいかない。そのため、一番近いモデルのハンドルをもらって、それとプレイヤーは当たり判定を行う形にすれば、円滑である。
	//追記理由：おまけに、絶対に当たらない位置にプレイヤーがいたときは、−１をもらえば、レイキャストをしなくてよい状態にできる。そうすればレイキャストの処理をしなくて済む
	//引数:地面のモデルハンドル
	//戻値:なし
	//プレイヤーに一番近い地面モデルのハンドルをもらう
	//プレイヤーと地面との衝突判定時に使用する地面のモデルハンドルとなる
	//−１をもらったとき、プレイヤーは、地面と当たり判定をしなくて大丈夫な位置にいるという判断になる
	void ChangeGroundModelHundle(int hGroundModel);

	//伊達追記
	//追記理由：ウツボのモデルは複数存在する、その中で、すべてと当たり判定を行うわけにはいかない。そのため、一番近いモデルのハンドルをもらって、それとプレイヤーは当たり判定を行う形にすれば、円滑である。
	//追記理由：おまけに、絶対に当たらない位置にプレイヤーがいたときは、−１をもらえば、レイキャストをしなくてよい状態にできる。そうすればレイキャストの処理をしなくて済む
	//引数:ウツボのモデルハンドル
	//戻値:なし
	//プレイヤーに一番近いウツボモデルのハンドルをもらう
	//プレイヤーと地面との衝突判定時に使用するウツボのモデルハンドルとなる
	//−１をもらったとき、プレイヤーは、ウツボと当たり判定をしなくて大丈夫な位置にいるという判断になる
	void ChangeMorayeelModelHundle(int hMorayellModel);

	//世界の地面モデルハンドルを取得
	//引数：世界の地面モデルハンドル
	//戻値：なし
	void SetWorldGroundModelHundle(int hWorldGround);


	void OnCollision(GameObject* pTarget) override;
};