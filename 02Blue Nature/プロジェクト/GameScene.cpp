#include "GameScene.h"
#include "Engine/Input.h"
#include "Engine/Direct3D.h"
#include "TitleScene.h"
#include "FishAssets.h"
#include "Animation.h"
#include "Player.h"
#include "ButterflyFish.h"
#include "Ground.h"
#include "SeaLevel.h"
#include "ScreenShot.h"
#include "GameMetaAI.h"


//ロード画面描画のため
#include "Engine/Direct3D.h"



//コンストラクタ
GameScene::GameScene(GameObject * parent)
	: GameObject(parent, "GameScene")
{
}

//デストラクタ
GameScene::~GameScene()
{
}

//初期化
void GameScene::Initialize()
{
	//背景色をセット
	//メタAI（SeaOperateMeta.cpp）にて、プレイヤー位置による背景色の計算を行わせている
	Direct3D::SetClearColor(0.1f, 0.5f, 0.8f, 1.0f);

	Instantiate<SeaLevel>(this);						//海面の生成
	//※必ずオブジェクト生成の先頭に

	GameObject* player = Instantiate<Player>(this);		//プレイヤーの生成

	//ロード画面描画のための準備
	Direct3D::BeginDraw();



	//メタAIの初期化
	GameMetaAI::Initialize(this, player);
	//�@地面生成指揮メタAI
	//�Aキャラクタ指揮メタAI

	
	Instantiate<ButterflyFish>(this);
	Instantiate<ScreenShot>(this);						//スクショの生成
	//GameMetaAI→GroundOperateMetaAIに移動（Instantiate<Ground>(this);）		//地面の生成
	

	TitleScene* titleScene = Instantiate<TitleScene>(this);		//タイトルシーンの生成
	titleScene->SetPlayer(player);								//プレイヤー情報を渡す
	player->Leave();											//プレイヤーの動きを止める
}

//更新
void GameScene::Update()
{
	//メタAIの更新
	GameMetaAI::Update();
}

//描画
void GameScene::Draw()
{
}

//開放
void GameScene::Release()
{
	//解放
	GameMetaAI::Release();
}