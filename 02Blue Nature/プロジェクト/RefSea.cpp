#include "RefSea.h"
#include "Engine/Model.h"

namespace RefSea
{
	Texture* pTexture;
	int seaModel;
}

void RefSea::Initialize(int hModel)
{
	pTexture = new Texture;
	pTexture->Load("2DModel/SeaTexture.png");
	seaModel = hModel;
}

Texture * RefSea::GetTexture()
{
	return pTexture;
}

void RefSea::Release()
{
	delete pTexture;
}

int RefSea::GetTime()
{
	return Model::GetAnimFrame(seaModel);
}
