#pragma once
//インクルード
#include <map>
#include "Engine/GameObject.h"


//前方宣言
class Strategy;


//ゲームシーンを管理するクラス
class GameScene : public GameObject
{
public:
	//引数：GameObject*（親オブジェクト）
	//返値：なし
	//コンストラクタ
	GameScene(GameObject* parent);

	//引数:
	//戻値:
	//デストラクタ
	~GameScene();

	//引数：なし
	//返値：void
	//初期化
	void Initialize() override;

	//引数：なし
	//返値：void
	//更新
	void Update() override;

	//引数：なし
	//返値：void
	//描画
	void Draw() override;

	//引数：なし
	//返値：void
	//開放
	void Release() override;
};