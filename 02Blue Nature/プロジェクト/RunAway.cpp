#include "RunAway.h"
#include "FishBase.h"
#include <random>

//コンストラクタ
RunAway::RunAway(GameObject* pGameObject , std::string target)
	:speedMag_(20.f),MAX_ROTATE_(60.f), rotate_(0.f)
{
	pObject_ = pGameObject;
	pFish_ = dynamic_cast<FishBase*>(pObject_);

	targetName_ = target;

	pTarget_ = pObject_->FindObject(targetName_);	//どのオブジェクトから逃げるのか
}

//デストラクタ
RunAway::~RunAway() 
{

}

bool RunAway::ShouldExecute()
{
	//認識(衝突)していればTrue
	return pFish_->IsCongnition();
}

void RunAway::Execute() 
{
	
	//MAX値を指定してるが多分引っかかることはない
	if (rotate_ < MAX_ROTATE_)
	{
		if (pFish_->GetIsRightEye())
		{
			pObject_->SetRotateY(pObject_->GetRotate().vecY + pFish_->GetRotateSpeedY());
		}
		if (pFish_->GetIsLeftEye())
		{
			pObject_->SetRotateY(pObject_->GetRotate().vecY -pFish_->GetRotateSpeedY());
		}
		rotate_ += pFish_->GetRotateSpeedY();
	}
	else 
	{
		rotate_ = 0.f;
	}
	

	//前進
	XMVECTOR move = XMVectorSet(0, 0, pFish_->GetMoveSpeed() * speedMag_, 0);
	move = XMVector3TransformCoord(move, XMMatrixRotationX(XMConvertToRadians(pObject_->GetRotate().vecX)));
	move = XMVector3TransformCoord(move, XMMatrixRotationY(XMConvertToRadians(pObject_->GetRotate().vecY)));
	pObject_->SetPosition(pObject_->GetPosition() + move);
}
