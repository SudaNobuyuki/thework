#pragma once

//前方宣言
struct AnimationSet;


//魚のアニメーションを管理
namespace Animation
{
	//指定したモデル(魚)のアニメーションを行う関数
	void SwimFish(int modelHandle, AnimationSet& anime);
	
	//アニメーションリミット(制限)
	void LimitAnimation(int& nowFrame);
}