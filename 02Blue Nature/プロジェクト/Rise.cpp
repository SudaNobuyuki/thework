#include "Rise.h"
#include "FishBase.h"
#include "CharacterAI.h"


Rise::Rise(GameObject * pObject)
	:MAX_ROTATE_(31.f)
{
	pObject_ = pObject;

	pFish_ = dynamic_cast<FishBase*>(pObject_);

}

Rise::~Rise() 
{
}

bool Rise::ShouldExecute() 
{
	return pFish_->GetVerticalAngle() < MAX_ROTATE_ && !pFish_->IsCongnition();
}

void Rise::Execute() 
{
	
	pFish_->SetRotateX(pFish_->GetRotate().vecX - pFish_->GetRotateSpeedX());
}
