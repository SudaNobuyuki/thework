

/*
 *
 * Class	 :各シーンの呼び込み、切り替えを行うクラス
 * Writer	 : 伊達龍二
 * Day		 : 2020/09/09 ~ (Redmineのタスク開始日から)
 * その他記述すべきことがある場合の記述
 * (備考)	: None
 *
 *
 */

#pragma once
#include "global.h"
#include "GameObject.h"

//ゲームに登場するシーン
	//登場するシーンのすべてのIDを登録（シーン切り替えにおけるシーンの選択に用いる）
enum SCENE_ID
{
	SCENE_ID_SPLASH = 0,
	SCENE_ID_TITLE,
	SCENE_ID_GAME,
	SCENE_ID_CREDIT,

};

//-----------------------------------------------------------
//シーン切り替えを担当するオブジェクト
//-----------------------------------------------------------
//継承：GameObjectクラス
class SceneManager : public GameObject
{
public:

	//コンストラクタ
	//引数：parent	親オブジェクト（基本的にゲームマネージャー）(GameObject　ポインタ)
	SceneManager(GameObject* parent);

	//初期化（オーバーライド）
	//引数：なし
	//戻値：なし
	void Initialize() override;
	//毎フレーム更新（オーバーライド）
	//引数：なし
	//戻値：なし
	void Update() override;
	//毎フレーム描画（オーバーライド）
	//引数：なし
	//戻値：なし
	void Draw() override;
	//解放処理（オーバーライド）
	//引数：なし
	//戻値：なし
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：next	次のシーンのID（enum値（int））
	//戻値：なし
	void ChangeScene(SCENE_ID next);

private:
	SCENE_ID currentSceneID_;	//現在のシーン
	SCENE_ID nextSceneID_;		//次のシーン

};