#include "BoxCollider.h"
#include "SphereCollider.h"
#include "OrientedBoxCollider.h"
#include "Model.h"
#include "GameObject.h"


//コンストラクタ
Collider::Collider():
	pGameObject_(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
}


//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw(XMVECTOR position, XMVECTOR rotate)
{
	Transform transform;

	XMVECTOR objRotate = pGameObject_->GetRotate();
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(objRotate.vecY));
	XMVECTOR center = XMVector3TransformCoord(center_, matY);

	transform.position_ = XMVectorSet(position.vecX + center.vecX, position.vecY + center.vecY, position.vecZ + center.vecZ, 0);
	transform.scale_ = XMVectorSet(size_.vecX, size_.vecY, size_.vecZ, 0.0f);
	transform.rotate_ = XMVectorSet(rotate.vecX, rotate.vecY, rotate.vecZ, 0.0f);

	transform.Calclation();
	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_);
}

void Collider::Draw(XMVECTOR position) 
{
	Transform transform;

	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(pGameObject_->GetRotate().vecY));
	XMVECTOR center = XMVector3TransformCoord(center_, matY);

	transform.position_ = XMVectorSet(position.vecX + center.vecX, position.vecY + center.vecY, position.vecZ + center.vecZ, 0);
	transform.scale_ = XMVectorSet(size_.vecX, size_.vecY, size_.vecZ, 0.0f);
	
	transform.Calclation();
	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_);
}
