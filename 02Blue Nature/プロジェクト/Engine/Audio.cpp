//#include "Audio.h"
//#include <xaudio2.h>
//#include <vector>
//
//#define SAFE_DELETE_ARRAY(p) if(p) {delete[] p; p = nullptr;}
//
//namespace Audio
//{
//	IXAudio2* pXAudio = nullptr;
//	IXAudio2MasteringVoice* pMasteringVoice = nullptr;
//
//	//複数の音を扱えるようにする
//	struct AudioData
//	{
//		XAUDIO2_BUFFER buf = {};
//		IXAudio2SourceVoice** pSourceVoice = nullptr;
//		int svNum;//ソースボイスの数
//		int svCount;
//	};
//	std::vector <AudioData> audioDatas;
//
//	//チャンクデータの基本構造 
//	struct Chunk
//	{
//		char	id[4]; // チャンク毎のID
//		int32_t	size;  // チャンクサイズ
//	};
//
//	// RIFFヘッダー
//	struct RiffChunk
//	{
//		Chunk	chunk;   // "RIFF"
//		char	type[4]; // "WAVE"
//	};
//
//	// FMTチャンク
//	struct FormatChunk
//	{
//		Chunk		chunk; // "fmt "
//		WAVEFORMAT	fmt;   // 波形フォーマット
//	};
//}
//
//void Audio::Initialize()
//{
//	//COM
//	CoInitializeEx(0, COINIT_MULTITHREADED);
//	//本体
//	XAudio2Create(&pXAudio);
//	//マスターボイス
//	pXAudio->CreateMasteringVoice(&pMasteringVoice);
//}
//
////WAVEファイルの読み込み
////引数：fileName　ファイル名
////戻値：番号（ロードしたデータに番号を割り振る）
//int Audio::Load(std::string fileName, int svNum)
//{
//	//ファイルを開く
//	FILE* file = nullptr;
//	if (fopen_s(&file, fileName.c_str(), "rb") != 0) {
//		return -1;
//	}
//
//	//----------ここから少しずつ情報を抜き取る-------------
//	//RIFFの読み込み
//	RiffChunk riff;
//	//第四引数から第二引数のデータを第三引数個読み込み第一引数に格納
//	//第一引数：読み込みデータ格納先のポインタ
//	//第二引数：読み込みデータのバイトの長さ
//	//第三引数：読み込みデータの数
//	//第四引数：FILEポインタ
//	//戻り値：読み込んだデータ個数(バイトではない)
//	//エラー時：戻り値より小さな値
//	fread(&riff, sizeof(riff), 1, file);
//
//	//FMT(Format)チャンクの読み込み
//	FormatChunk format;
//	fread(&format, sizeof(format), 1, file);
//
//	// Dataチャンクの読み込み
//	Chunk data;
//	fread(&data, sizeof(data), 1, file);
//
//	//Dataチャンクのデータ部(波形データ)の読み込み
//	char* pBuffer = (char*)malloc(data.size);
//	fread(pBuffer, data.size, 1, file);
//
//	fclose(file);
//
//	//---------SourcceVoiceの生成-----//
//	WAVEFORMATEX wfex{};
//	//波形フォーマットの設定
//	memcpy(&wfex, &format.fmt, sizeof(format.fmt));
//	//1サンプル当たりのバッファサイズを算出
//	wfex.wBitsPerSample = format.fmt.nBlockAlign * 8 / format.fmt.nChannels;
//
//	//再生する波形データの設定
//	AudioData ad;
//	ad.buf.pAudioData = (BYTE*)pBuffer;
//	ad.buf.Flags = XAUDIO2_END_OF_STREAM;//デバック出力警告を抑制するため
//	ad.buf.AudioBytes = data.size;
//	//ad.buf.LoopCount = XAUDIO2_LOOP_INFINITE;
//
//	ad.pSourceVoice = new IXAudio2SourceVoice*[svNum];
//	for (int i = 0; i < svNum; i++)
//	{
//		pXAudio->CreateSourceVoice(&ad.pSourceVoice[i], &wfex);
//	}
//	ad.svNum = svNum;
//	audioDatas.push_back(ad);
//
//	/*SAFE_DELETE_ARRAY(pBuffer);*/
//
//
//	return audioDatas.size() - 1;
//
//}
//
//
//void Audio::Play(int ID)
//{
//	//5個のソースボイスの状態を調べ、暇そうなやつを見つける
//	for (int i = 0; i < audioDatas[ID].svNum; i++)
//	{
//		XAUDIO2_VOICE_STATE state;
//		//現状把握
//		audioDatas[ID].pSourceVoice[i]->GetState(&state);
//
//		//仕事が無い（キューに溜まってる情報が0）
//		if (state.BuffersQueued == 0)
//		{
//			//鳴らす
//			audioDatas[ID].pSourceVoice[i]->SubmitSourceBuffer(&audioDatas[ID].buf);
//			audioDatas[ID].pSourceVoice[i]->Start();
//			audioDatas[ID].svCount = i;
//			break;
//		}
//	}
//}
//
//void Audio::Destory(int ID)
//{
//	audioDatas[ID].pSourceVoice[audioDatas[ID].svCount]->DestroyVoice();
//
//}
//
////void Audio::Endplay(int ID)
////{
////	BOOL isrunning;
////	XAUDIO2_VOICE_STATE state;
////	//現状把握
////	audioDatas[ID].pSourceVoice[audioDatas[ID].svCount]->GetState(&state);
////
////	if(state.BuffersQueued>0){}
////
////
////
////}
//
//void Audio::Release()
//{
//	//for (int i = 0; i < audioDatas.size; i++)
//	//{
//	//	for (int j = 0; j < audioDatas[i].svNum; j++)
//	//	{
//	//		audioDatas[i].pSourceVoice[j]->DestroyVoice();
//	//		audioDatas[i].pSourceVoice[j] = nullptr;
//	//	}
//	//}
//	if(pMasteringVoice)
//	pMasteringVoice->DestroyVoice();
//
//	if (pXAudio) {
//		pXAudio->Release();
//		pXAudio = nullptr;
//	}
//
//	//SAFE_DELETE_ARRAY(pXAudio);
//
//	CoUninitialize();
//}
