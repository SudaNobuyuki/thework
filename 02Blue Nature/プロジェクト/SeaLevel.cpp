#include "SeaLevel.h"
#include "Engine/Model.h"
#include "RefSea.h"

//コンストラクタ
SeaLevel::SeaLevel(GameObject * parent)
	:GameObject(parent, "SeaLevel")
{
}

//デストラクタ
SeaLevel::~SeaLevel()
{
}

//初期化
void SeaLevel::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("3DModel/SeaLevel3.fbx");
	assert(hModel_ >= 0);

	transform_.scale_ *= 512.0f;
	transform_.scale_.vecY /= 2.0f;
	transform_.position_.vecY = 200.0f;

	Model::SetAnimFrame(hModel_, 0, 10000, 1.0f);

	SetSea();

	//海面反射のテクスチャ初期化
	RefSea::Initialize(hModel_);
}

//更新
void SeaLevel::Update()
{
}

//描画
void SeaLevel::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void SeaLevel::Release()
{
	RefSea::Release();
}