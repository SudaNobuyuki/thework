#include "CreditScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
CreditScene::CreditScene(GameObject * parent)
	: GameObject(parent, "CreditScene"), hPict_(-1)
{
}

//初期化
void CreditScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Credit/Credit.png");
	assert(hPict_ >= 0);

	//初期値
	transform_.position_.vecY = -4.5f;
}

//更新
void CreditScene::Update()
{
	//画像スクロール
	transform_.position_.vecY += 0.005f;

	//スクロールを早める
	if(Input::IsKey(DIK_RETURN) || Input::IsMouseButton(0x00))transform_.position_.vecY += 0.005f;

	//クレジット強制終了
	if (Input::IsKey(DIK_SPACE))transform_.position_.vecY = 100;

	//クレジット終了
	OutputDebugString((std::to_string(transform_.position_.vecY) + "\n").c_str());
	if (transform_.position_.vecY > 3.65f)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAME);
	}
}

//描画
void CreditScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void CreditScene::Release()
{
}
