#pragma once
#include "MetaBase.h"


//Gの上昇値
#define UP_VALUE_GREEN 35.0f
//Gの下昇値
#define DOWN_VALUE_GREEN 25.0f

class SeaOperateMeta :
	public MetaBase
{
	////シーンのオブジェクトポインタ
	//GameObject* pSceneObject_;
	
	//プレイヤーオブジェクトポインタ（地面のオブジェクトは１つで、そのオブジェクト内に複数の地面モデルを取得する）
	GameObject* pPlayer_;

	//フレームカウンタ‐
	//毎フレーム行ってもそこまで変化は望めない。そのため、一定フレーム後に計算をするため、そのためのカウンター
		//時間で計測するほどでもない
		//一定フレームを超えたら処理を行うようにする
		//毎フレーム行うことでもないと思うため、フレーム計測でUpdateの処理を行うかを決める
		//そもそも処理数が少ないため、条件式一つを付けるのもどうかと思うが、、
	int frameCounter_;


	//計算を開始するフレーム数
	const int CALC_FRAME_;


	//通常の背景色ノーマルカラー
		//プレイヤーが（０，０，０，０）にいるときの背景色
	const XMVECTOR NORMAL_COLOR_;

	//背景色を求める差に用いる値
		//Yの最高値を値として持っておく
	const float posY_;


	//色の変化値
	const float changeValue_;


	//プレイヤーの現在位置から
	//背景色を変化させる
	void CalculationColor();

public : 
	//コンストラクタ
	//引数：なし
	//戻値：なし
	SeaOperateMeta();
	//デストラクタ
	//引数：なし
	//戻値：なし
	~SeaOperateMeta();

	// MetaBase を介して継承されました
	
	// MetaBase を介して継承されました
	//初期化（地面オブジェクトの生成）
	//引数：シーンのオブジェクトポインタ
	//引数：プレイヤーのオブジェクトポインタ（自身の指揮メタAIでは、プレイヤーオブジェクトは使用しないが、ベースクラスにキャストした際に一度に呼べるように関数形式はどのクラスでも共通した形にした。）
	//戻値：なし
	void Initialize(GameObject * pSceneObject, GameObject * pPlayer) override;
	
	//更新
	//引数：管理下のオブジェクトリスト（毎フレーム更新として何かの指揮をする場合は、このリストに、そのオブジェクト群を送ってもらう（その際、リストへの追加は、地面オブジェクトがメタAIにリストへの追加を呼び込む（魚指揮メタAIと同じ）呼び方を行う））
			//指揮メタAIから、メタAIのリストに追加する形は、理想ではない。
	//戻値：なし
	void Update(std::list<GameObject*>& lookObject) override;

	//解放
	//引数：なし
	//戻値：なし
	void Release() override;
};

