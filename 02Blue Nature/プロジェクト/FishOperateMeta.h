#pragma once
/*
*
	指揮メタAI
	魚指揮メタAI
* 魚の生成から、魚の消去を行う
* 作者名：伊達龍二
* 制作日：2020/10/20〜
* その他記述すべきことがある場合の記述
*		魚の生成種類、生成場所に条件を付けて生成
		プレイヤー一定範囲内に、魚がいすぎないようにコストによる制限
		おなじ魚がいすぎないように生成管理
		プレイヤー一定範囲以上離れたら、オブジェクトの消去

*/


#include <stdlib.h>
#include "MetaBase.h"



//前方宣言
class FishCsvInfo;
enum FISH_TYPE;
class RayCastData;
class FishBase;

//※親のGameSceneのサイズを変更したとしても、以下のサイズをそれに対して変える処理は必要ない→なぜならば、Transformクラスにて、
	//→オブジェクトのTransformを、親のTransformの各行列を自らの行列に掛けて表示しているため、（つまり親のワールド行列を掛けたワールド行列が自身のワールド行列となる。）
	//→となれば、親のTransformが変更されれば、子供のTransformも変更される。（親が５倍になれば、子供も５倍になる。親がｘに移動すれば、子供もｘに移動する）
//描画範囲の球の大きさ（半径）(プレイヤー原点としての球の半径)
	//ワールド値（ワールド上の座標値に等しい(ワールド上のTransform.position_、かつ親となるオブジェクトの拡大率なども考慮した座標値に)）
//X方向
#define VIEW_RADIUS_X_ 300
//Y方向
#define VIEW_RADIUS_Y_ 500
//Z方向
#define VIEW_RADIUS_Z_ 300
//プレイヤーを原点として、オブジェクトを表示させたくない範囲（半径）
	//視野範囲内時
#define VIEW_NOT_POS_ 20

//指揮範囲の球の大きさ（指揮範囲の範囲は、実際、描画範囲を除いた範囲が指揮範囲となる）
	//ワールド値（ワールド上の座標値に等しい）
//X方向
#define FIELD_RADIUS_X_ (VIEW_RADIUS_X_* 2)
//Y方向
#define FIELD_RADIUS_Y_ (VIEW_RADIUS_Y_* 2)
//X方向
#define FIELD_RADIUS_Z_ (VIEW_RADIUS_Z_* 2)

//プレイヤーを原点として、オブジェクトを表示させたくない範囲（半径）
	//視野範囲内時
#define FIELD_NOT_POS_ VIEW_NOT_POS_ * 3



//キャラクタ関連
//更新を開始する距離
#define UPDATE_RANGE_ 1000

//描画を開始する距離
#define DRAW_RANGE_ 1000


class FishOperateMeta :
	public MetaBase
{
private:
	//出現位置のタイプ
	enum FISH_DISP_TYPE
	{
		VIEW_RADIUS,	//視野範囲内　に出現させる
		FIELD_RADIUS,	//描画範囲内　に出現させる（視認できない位置）

		MAX_DISP_TYPE,

	};

	enum TYPE
	{
		UPDATE_TYPE_ = 0,
		DRAW_TYPE_,

		TYPE_END_
			
	};


	//魚合計数
	const int FISH_COUNT_;

	//魚オブジェクトが出現されるたびにカウントされていくコスト
		//魚オブジェクトごと
		//オブジェクトコスト / 2
		//配列ポインタにて実体をFISH_COUNT_分確保する
	int* objectDispCounter_;



	//海面のY座標値を保存（海面の高さが共通前提）
	const float SEA_POS_Y_;


	/*保存用コスト*/
	//描画範囲内コスト最大（魚一匹ずつにそれぞれコストを持たせておいて、そのコストを加算してプレイヤー一定範囲のコスト管理とする）
		//描画範囲内 = プレイヤーから視認できる範囲（カメラの後方も含む（カメラから見えていない範囲も含めての、一定距離））
	const int VIEW_COST_MAX_;
	//指揮範囲内コスト最大（描画範囲内）
			//指揮範囲内 = プレイヤーから視認できない範囲、かつ、メタAIにより指揮を行うオブジェクトリストに追加される範囲（カメラの後方も含む（カメラから見えていない範囲も含めての、一定距離））
	const int FIELD_COST_MAX_;

	//変動用コストと保存用コストとの差分（差をとって、この値までは、よしとする。この値までは、超えてない、下回っていないと判断する）
	const int DEAD_SPACE_;


	/*変動用コスト*/
	//描画範囲内コスト
	//プレイヤー一定範囲内(プレイヤーからの描画範囲内)魚を発生させておける合計コスト
	int viewCost_;
	//指揮範囲内コスト
	//一定範囲内（プレイヤー視野範囲外の、キャラクタの削除、プレイヤー視野範囲内予備軍）
	//の魚発生合計コスト
	int fieldCost_;
	/*
		コスト基準：（コスト選定基準（オブジェクトのコスト値の設定基準））
			・ポリゴン数が多い→高コスト
			・ポリゴン数が少い→低コスト
			・レアリティが高い→高コスト（滅多に現れない）
			・レアリティが低い→低コスト（頻繁に現れる、モブ）
			・環境不適合      →高コスト（その環境に出現するのは不適合）
			・環境適合        →低コスト
			・エリアの優先度  → ーコスト（このエリアはこの魚（環境に近い）出してほしい）
			・状態優先度      → ーコスト（今は、この魚出現して）

		などで、CSVファイルにコストや、優先度を持っておき、出現オブジェクト選定時にコストの計算を行い
		すべてのオブジェクトの、すべてのコスト計算が終わった段階で、一番低コストのオブジェクトを今回の出現オブジェクトとする。
	*/




	//CSV情報からオブジェクトを生成するクラス（Instantiate関数を所有しておくクラス）
	FishCsvInfo* pFishCsvInfo_;


	//メタAI適用シーンオブジェクト
	GameObject* pSceneObject_;
	//プレイヤーオブジェクト(プレイヤーオブジェクトは変更されないので、所有しておく（ゲームオーバーも）ないので、)
	GameObject* pPlayer_;


	//指定範囲内のランダムの座標にオブジェクトを移動させるための座標を返す関数
	//引数：出現範囲を示すタイプ
	//引数：x方向の指定範囲　半径
	//引数：y方向の指定範囲　半径
	//引数：z方向の指定範囲　半径
	//戻値：引数にてもらった範囲内でかつ、プレイヤー中心の一定範囲内を除いたランダム座標位置
	XMVECTOR SetRandomPosition(FISH_DISP_TYPE dispType, int radiusX, int radiusY, int radiusZ);

	//ランダムで符号（+OR- のため値として-１OR１を返す）
	//引数：なし
	//戻値：ランダムに取得された符号を示す値
	int RandCode();

	//地面へ向けて、レイを飛ばし、引数にて渡した座標を地面に沿わせる処理を行う
	//引数：地面に沿わせる、ベクトル
	//引数：レイのデータ
	//引数：符号データ
	//戻値：地面との衝突判定を成功させたフラグ
	bool GroundMatch(XMVECTOR* matchPos,
		int modelHundle, RayCastData* pData, int code);


	//出現最優先となる魚オブジェクト選定
	//引数：出現範囲を示すタイプ
	//戻値：オブジェクトを識別するenum値
	void GetTopPriorityObj(XMVECTOR& randPos, int& groundPlate, FISH_TYPE& fish , FISH_DISP_TYPE dispType);


	//引数ベクトルに一番近い地面プレートを出す
		//ベクトルと一番近い地面のワールド座標（オブジェクトのTransform,position）の地面番号を取得
	//引数：判定ベクトル
	//戻値：地面プレート番号
	int GetNearGroundPlate(const XMVECTOR& pos);

	//オブジェクトの標準のコストを取得
		//引数にて渡された値から、オブジェクトのタイプを選別し、そのオブジェクトの標準コストを
		//CSVファイルから読み取る
	//引数：配列の先頭アドレスをポインタで受け取る
	//引数：オブジェクトのタイプ（配列の添え字としての番号のため、オブジェクトのタイプとしては１オリジンのため＋１した値を使用する。）
	//戻値：なし
	void GetObjectDefaultCost(int costByObject[], const int objectType);

	//地面のプレート番号から、オブジェクトごとの優先度を出し
		//優先度からコストの計算を行う
		//優先度が高ければ高いほど、コストを減らす
	//引数：コストを取得しておく配列の先頭アドレス受け取り
	//引数：オブジェクトのタイプ（配列の添え字としての番号のため、オブジェクトのタイプとしては１オリジンのため＋１した値を使用する。）
	//引数：地面のプレート番号
	//戻値：なし
	void PriorityCalcOfObjectCost(int costByObject[], const int objectType, const int groundPlateNum);

	//優先度込みのオブジェクトのコストの計算を終えたモノの中で
		//一番コストの低いオブジェクトを選定
	//引数：コストを取得しておく配列の先頭アドレス受けとり
	//戻値：生成するオブジェクトのタイプ(enum値)
	FISH_TYPE GetMinCostObjectType(int costByObject[]);


	//１匹のオブジェクトの生成
		//生成するオブジェクトの選定（特定条件によりランダムに選定）
		//オブジェクトの位置の決定
	//引数：出現範囲を示すタイプ
	//戻値：なし
	void CreateOneObject(FISH_DISP_TYPE dispType);


	void UpdateAndDrawExe(GameObject* pGameObject);


	//引数：モデル番号（添え字）
	//引数：範囲内距離
	void InRange(GameObject* pGameObject , int range, TYPE type);


public:
	//コンストラクタ
	//引数：なし
	//戻値：なし
	FishOperateMeta();
	//デストラクタ
	//引数：なし
	//戻値：なし
	~FishOperateMeta() override;

	// MetaBase を介して継承されました
	//初期化（地面オブジェクトの生成）
	//引数：シーンのオブジェクトポインタ
	//引数：プレイヤーのオブジェクトポインタ
	//戻値：なし
	void Initialize(GameObject* pSceneObject, GameObject* pPlayer) override;
	//更新
	//引数：指揮メタAIにて指揮するオブジェクト群のリスト（参照渡し）
	//戻値：なし
	void Update(std::list<GameObject*> &lookObject) override;
	//解放
	//引数：なし
	//戻値：なし
	void Release() override;


	//プレイヤー現在位置から、一定範囲を調べ範囲を超えたオブジェクトを参照し、消去
	//引数：指揮メタAIにて指揮するオブジェクト群のリスト（参照渡し）
	//戻値：なし
	void SearchFieldKillObject(std::list<FishBase *> &lookingObject);
	//消去した分新たに、プレイヤー現在位置から、一定範囲内にオブジェクトの出現
	//引数：出現範囲を示すタイプ（視野範囲内（ゲーム開始時）、描画範囲内（シーン実行時））
	//戻値：なし
	void RefillObject(FISH_DISP_TYPE dispType);


	//１匹の指定オブジェクトの生成
		//生成するオブジェクトの選定（特定条件によりランダムに選定）
		//オブジェクトの位置の決定
	//引数：出現範囲を示すタイプ
	//戻値：なし
	void CreateThisObject(FISH_TYPE fishType);




	//コストの加算
	//自身の持つコスト合計値に引数の値を足す
	//各継承先にて、自身の所有しているコスト合計値に加算する処理（コストを持ち合わせない場合、継承先で、何もオーバーライドしなければよい）
	//引数：加算するコスト（どのオブジェクトのコストであるかは関係なし（オブジェクトごとの識別に関してはAddObjectCost関数にて処理を行う（詳しくはその関数にて）））
	//戻値：なし
	void AddCost(int addCost) override;	//継承元では何も書かない→継承先でオーバーライドしなければ、関数が呼ばれたときに、何もしない処理になる。

	//オブジェクト別のコストの加算
	//オブジェクトが１体出現されるたびにオブジェクトごとのコスト（オブジェk津尾の持っている標準のコストとは別）が加算される
		//オブジェクトの出現をランダム性を高める為に、（おなじオブジェクトが出現しすぎないように）
		//オブジェクトが１体出現されたら、自身のメンバに持っている、コストのカウンターにオブジェクトごとのコストの加算を行う（それをオブジェクト出現時の、出現オブジェクト選定のためのコスト計算などに使用する）
	//引数：コストの加算を行うオブジェクトのタイプ（int）
	//引数：コスト値
	//戻値：なし
	void AddObjectCounter(int type, int addCost) override;


	//コストの減算
	//自身の持つコスト合計値に引数の値を引く
	//引数：減算するコスト（どのオブジェクトのコストであるかは関係なし）
	//戻値：なし
	void SubCost(int subCost) override;

	//オブジェクト別のコストの減算
		//加算した分のコストを減算
	//引数：コストの減算を行うオブジェクトのタイプ（int）
	//引数：コスト値
	//戻値：なし
	void SubObjectCounter(int type, int subCost) override;
};

