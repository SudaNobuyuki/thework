#include "GetCloser.h"
#include "FishBase.h"

//コンストラクタ
GetCloser::GetCloser(GameObject * pGameObject, std::string target) :moveSpped_(5.5f), cutAngle_(0.0f), cut_(10.0f),
rotateAngle_(0.0f), arrive_{ 0,0,5,0 }, ownDir_{ 0,0,1,0 }, randam_(0)
{
	pObject_ = pGameObject;
	//dynamic_cast < type-id > ( expression )
	// <>の中で、変換する目的の型を指定する　※必ずポインタ型か参照でなければならない
	//()の中では、目的の型に変換する式を指定します　※<>の中身が、ポインタ型ならポインタ、参照なら変数を指定
	//pObject_型をFishBase型に変換して、pFish_に代入している
	//派生クラスのオブジェクトを基本クラス型に変換→アップキャスト
	//基本クラスのオブジェクトを派生クラス型に変換→ダウンキャスト
	pFish_ = dynamic_cast<FishBase*>(pObject_);

	//名前を設定
	targetName_ = target;

	//近づく相手を設定
	pTarget_ = pObject_->FindObject(targetName_);	//どのオブジェクトに近づくのか
}

//デストラクタ
GetCloser::~GetCloser()
{

}

//実行可能であれば
bool GetCloser::ShouldExecute()
{
	//認識(衝突)していればTrue
	//return pFish_->IsCongnition();

	//ここの部分に近づきたい相手との距離が一定以下になった場合か、目視した場合に、
	//認識フラグを更新する処理を追加すればよい
	//目視していればTrue
	return pFish_->IsVisually();
}

//実行
void GetCloser::Execute()
{
	//乱数の初期化
	randam_ = 0;

	//乱数の種
	srand((int)time(NULL));

	//乱数生成(0〜1)
	randam_ = rand() % 2;

	//自身の位置
	ownPos_ = pObject_->GetPosition();
	//相手の位置
	tarPos_ = pTarget_->GetPosition();

	//目標地点
	tarPos_ = tarPos_ + arrive_;


	//距離
	disPos_ = tarPos_ - ownPos_;
	//自身の方向ベクトル
	ownDir_ = XMVector3TransformCoord(ownDir_, XMMatrixRotationX(XMConvertToRadians(pObject_->GetRotate().vecX)));
	ownDir_ = XMVector3TransformCoord(ownDir_, XMMatrixRotationY(XMConvertToRadians(pObject_->GetRotate().vecY)));

	//回転角度
	disRotate_ = disPos_ - ownDir_;

	//回転したい角度
	rotateAngle_ = disRotate_.vecZ;
	//角度を分割して回転させるためのもの
	cutAngle_ = rotateAngle_ / cut_;


	//移動
	move_ = XMVectorSet(0, 0, pFish_->GetMoveSpeed(), 0);
	move_ = XMVector3TransformCoord(move_, XMMatrixRotationX(XMConvertToRadians(pObject_->GetRotate().vecX)));
	move_ = XMVector3TransformCoord(move_, XMMatrixRotationY(XMConvertToRadians(pObject_->GetRotate().vecY)));
	move_ = move_ * moveSpped_;


	//乱数が(1)の時だけ近づく処理をする
	if (randam_ == 1)
	{
		//右目の範囲の処理
		if (pFish_->GetIsRightEye())
		{
			//回転を適用
			pObject_->SetRotateY(pObject_->GetRotate().vecY + cutAngle_);
			pObject_->SetPosition(pObject_->GetPosition() + move_);
		}

		//左目の範囲の処理
		if (pFish_->GetIsLeftEye())
		{
			//回転を適用
			pObject_->SetRotateY(pObject_->GetRotate().vecY - cutAngle_);
			pObject_->SetPosition(pObject_->GetPosition() + move_);
		}
	}
}