
#include "SplashScene.h"

#include <cmath>
#include "Engine/Image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Timer.h"
#include "Engine/Audio.h"


//コンストラクタ
SplashScene::SplashScene(GameObject * parent)
	: GameObject(parent, "SplashScene"), RESOURCE_PATH_("Logo\\"), elapsedTime_(0.0f)
	, LOGO_TRANSITION_TIME_(6.0f), SPLASH_FINISH_TIME_(12.0f), FADE_OUT_RATIO_(0.5f), FADE_IN_SMOOTHNESS_(0.75f)
	, START_POS_SHIFT_(1.5f), UV_X_(6), UV_Y_(6), CUT_WIDTH_(500), CUT_HEIGHT_(500), audioBuble_(-1)
{
	hPicts_.fill(-1);
	itr_ = hPicts_.begin();

}

//初期化
void SplashScene::Initialize()
{
	//ロゴを管理する配列。格納順に表示される。
	std::array<std::string, 2> logos = { "TeamLogo.png","Tohokudenshi.png" };

	//画像ロード
	for (int i = 0; i < logos.size(); i++)
	{
		hPicts_[i] = Image::Load(RESOURCE_PATH_ + logos[i]);
		assert(hPicts_[i] >= 0);
	}

	//背景色をセット
	Direct3D::SetClearColor(0.f, 0.f, 0.f, 1.f);

	//SceneManagerを探してポインタを格納
	pSceneManager_ = (SceneManager*)FindObject("SceneManager");

	//仮Audioのロード
	//audioBuble_ = Audio::Load("SE/awa_mono.wav");
	//assert(audioBuble_ >= 0);
}

//更新
void SplashScene::Update()
{
	//経過時間を加算
	elapsedTime_ += Timer::GetDelta();

	//Enterキーを押したら時間を進める(スキップ機能)
	if (Input::IsKeyDown(DIK_RETURN) || Input::IsMouseButtonDown(0x00))
	{
		//elapsedTime_ = elapsedTime_ > LOGO_TRANSITION_TIME_ ? SPLASH_FINISH_TIME_ : LOGO_TRANSITION_TIME_;
		if (elapsedTime_ > LOGO_TRANSITION_TIME_)
		{
			elapsedTime_ = SPLASH_FINISH_TIME_;
		}
		else
		{
			elapsedTime_ = LOGO_TRANSITION_TIME_;

		}
	}


	//経過時間がCHANGE_TIME_より大きい、かつitr_がhPicts_.begin()であれば、itr_を進める
	if (elapsedTime_ > LOGO_TRANSITION_TIME_ && itr_ == hPicts_.begin())
	{
		++itr_;
	}

	//経過時間がFINISH_TIME_より大きければ、タイトルシーンに遷移
	if (elapsedTime_ > SPLASH_FINISH_TIME_)
	{
		//Audio::Play(audioBuble_);
		/*ここでタイトルシーンに遷移*/
		pSceneManager_->ChangeScene(SCENE_ID_GAME);
	}
}

//描画
void SplashScene::Draw()
{
	//透過率を設定
	float alpha = (sin(elapsedTime_ - START_POS_SHIFT_ + cos(elapsedTime_ - START_POS_SHIFT_)) + FADE_OUT_RATIO_) * FADE_IN_SMOOTHNESS_;

	//透過率をセット
	Image::SetAlpha(*itr_, alpha);

	//指定範囲の切り取り(残った枠の除外のため)
	//Image::SetRect(*itr_, UV_X_, UV_Y_, CUT_WIDTH_, CUT_HEIGHT_);

	//変形
	Image::SetTransform(*itr_, transform_);

	//描画
	Image::Draw(*itr_);
}

//開放
void SplashScene::Release()
{
}
