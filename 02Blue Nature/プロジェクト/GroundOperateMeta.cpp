#include "GroundOperateMeta.h"

#include "Engine/Global.h"
#include "Engine/GameObject.h"	//Instantiate、オブジェクトを生成するため

#include "GameMetaAI.h"
#include "MorayEElOperateMeta.h"	//ウツボ生成クラス
#include "Ground.h"				//地面オブジェクト
#include "Player.h"				//プレイヤーオブジェクト




GroundOperateMeta::GroundOperateMeta() :
	pSceneObject_(nullptr),
	pGroundObject_(nullptr),
	pPlayer_(nullptr),
	pMorayEelMeta_(nullptr)
{
	for (int i = 0; i < GROUND_MAX; i++)
	{
		groundModelHundle_[i] = -1;
	
	}

	////vector可変長配列初期化
	//groundModelHundle_.clear();

}

GroundOperateMeta::~GroundOperateMeta()
{
}

void GroundOperateMeta::Initialize(GameObject * pSceneObject, GameObject * pPlayer)
{
	pSceneObject_ = pSceneObject;


	//地面モデルは、
	//ひとつのクラスで、複数地面のプレートを読み込む形になっている。
	//地面の配置の仕方
		//�@（０，０、０）のプレートを始点として
		//　�A�@を左上として〇×〇のプレート配置
		//　�A�@を中心として〇×〇のプレート配置



	//地面情報を登録
	//地面のモデルの生成
	pGroundObject_ = Instantiate<Ground>(pSceneObject_);

	//初期地面のモデル番号の取得
	//groundModelHundle_[GROUND_0 - 1] = pGroundObject_->GetModelHundle();

	//地面モデル番号の取得
	for (int i = 0; i < GROUND_MAX; i++)
	{
		//引数にてモデルの識別番号を送り
		//戻値として、モデルの番号ハンドルをもらう
		groundModelHundle_[i] = pGroundObject_->GetOneModelHundle(i);
	}

	//世界のモデル番号の取得
	hWorldGround_ = pGroundObject_->GetWorldGroundModelHundle();
	

	

	//メンバ変数に
	//プレイヤーのポインタ取得
	pPlayer_ = pPlayer;

	//プレイヤーに、当たり判定用のモデルとして知らせる
	//地面モデル番号
	Player* pPlayer2 = (Player*)pPlayer_;
	pPlayer2->ChangeGroundModelHundle(groundModelHundle_[GROUND_0 - 1]);
	//世界の地面モデル番号
	pPlayer2->SetWorldGroundModelHundle(hWorldGround_);




	//groundModelHundle_.push_back(pGroundObject_->GetModelHundle());


	//ウツボ生成クラスの動的確保
	pMorayEelMeta_ = new MorayEElOperateMeta;
	//初期化の呼び出し
	pMorayEelMeta_->Initialize(pSceneObject, pPlayer);
	//初期地面のモデル番号におけるウツボの生成
	pMorayEelMeta_->LoadMorayEel(GROUND_0 - 1, groundModelHundle_[GROUND_0 - 1]);








}

void GroundOperateMeta::Update(std::list<GameObject*>& lookObject)
{
	//UpdateLoadGround();

	ChangeRayCastModel();

	//ウツボメタAIのUpdateを呼び込む
		//プレイヤーに一番近いウツボモデルの選定
	//引数の値は仮なので、
	//関数先で使うことはない。
	pMorayEelMeta_->Update(lookObject);

}

void GroundOperateMeta::Release()
{
	SAFE_DELETE(pMorayEelMeta_);


}

int GroundOperateMeta::GetModelHundle(GROUND_PLATE_HUNDLE groundHundle)
{
	//引数にて指定された値の地面のモデル番号（ハンドル）を返す
		//引数の値は1オリジン
		//ベクター配列は0オリジンなので、
	return groundModelHundle_[groundHundle - 1];

}


void GroundOperateMeta::UpdateLoadGround()
{
	////生成完了、未完了表
	//	//この表に、作成済みか、済でないかのフラグを入れる。
	//bool isCreate[];


	//生成を完了していない地面群を取得
	for (int i = 0; i < GROUND_MAX; i++)
	{
		//モデルハンドルがー１なら生成済みでない。
		if (groundModelHundle_[i] == -1)
		{
			//距離を調べて、生成できる距離であるならば、生成

			//CSVから地面の座標を取得
				//0オリジンで渡す
				//関数先にて、1オリジンに直してくれる
			XMVECTOR groundPos = GameMetaAI::GetGroundPos(i);
			groundPos = XMVectorSet(groundPos.vecX, groundPos.vecY, groundPos.vecZ, 0);


			//プレイヤーの現在値取得
			XMVECTOR playPos = pPlayer_->GetPosition();
			playPos = XMVectorSet(playPos.vecX, playPos.vecY, playPos.vecZ, 0);


			//距離をY座標抜きで行うために
			//地面のY座標をプレイヤーのY座標に合わせる
			groundPos.vecY = playPos.vecY;


			//プレイヤーと地面との距離をとる
			float length = XMVector3Length(groundPos - playPos).vecX;

			//生成可能距離よりも下回ったら
			if (length <= IS_CREATE_DISP)
			{
				//地面生成
				LoadGround(i);

				//地面を生成完了したので、
				//その地面に存在するウツボオブジェクトも生成させる（その地面にウツボオブジェクトが存在するならば、（ウツボの出現位置は、地面プレート内にランダムで決めている））
				pMorayEelMeta_->LoadMorayEel(i, groundModelHundle_[i]);


			}


		}


	
	}

}

void GroundOperateMeta::LoadGround(int createNum)
{
	//地面クラスの
	//Loadクラスを呼びこむ

	//戻り値で地面のモデルハンドル番号を受け取る
				//戻値は必ず、
					//−１以外のモデルハンドル番号になる（−１の時点でLoad時に警告発するようになっている）
	groundModelHundle_[createNum] = pGroundObject_->LoadGround(createNum);


}

//プレイヤーの座標を確認して、
//プレイヤーとレイキャストの当たり判定を行う地面番号を与える。
void GroundOperateMeta::ChangeRayCastModel()
{
	//プレイヤーの座標を調べる
	//プレイヤーの座標がそもそも、地面には絶対に触れない高さ
		//地面のY座標が４００なので、プレイヤーのY座標がー100程度であれば、絶対に地面には触れない。なので、その時点で、プレイヤーには、
		//レイでの衝突判定を行わないようにさせる



	Player* pPlayer = (Player*)pPlayer_;
	/*pPlayer->ChangeGroundModelHundle(groundModelHundle_[GROUND_0 - 1]);
	return;*/


	//pPlayer->ChangeGroundModelHundle(groundModelHundle_[0]);
	//return;


	//プレイヤーの現在位置を取得
	XMVECTOR playPos = pPlayer->GetPosition();

	if (playPos.vecY >= MIN_GROUND_POS_Y)
	{
		//−１を渡して、
		//レイキャストをしなくてよいとする
		pPlayer->ChangeGroundModelHundle(-1);
		//関数終了
		return;
	}

	int groundNumber = -1;

	float minLength = 9999999;


	//全地面の座標を受け取り、
	//プレイヤーと一番近い地面を受け取る
	for (int i = 0; i < GROUND_MAX; i++)
	{
		//地面がすでに生成済みであり
		if (groundModelHundle_[i] != -1)
		{
			//地面の位置を取得
			//XMVECTOR groundPos =  GameMetaAI::GetGroundPos(i);
			
			//地面プレートごとの座標を取得
			XMVECTOR groundPos = pGroundObject_->GetGroundPos(i);
			groundPos = XMVectorSet(groundPos.vecX, groundPos.vecY, groundPos.vecZ, 0);

			//プレイヤーの現在値取得
			XMVECTOR playPos = pPlayer_->GetPosition();
			playPos = XMVectorSet(playPos.vecX, playPos.vecY, playPos.vecZ, 0);

			//距離をY座標抜きで行うために
			//地面のY座標をプレイヤーのY座標に合わせる
			groundPos.vecY = playPos.vecY;


			//プレイヤーと地面との距離をとる
			float length = XMVector3Length(groundPos - playPos).vecX;

			//現在の最小の長さのほうが大きいとき
			if (minLength > length)
			{
				groundNumber = i;
				minLength = length;

			}


		}
	
	}

	//最小の長さを持つ地面を求められなかった
	if (groundNumber == -1)
	{
		//-1を返す
		pPlayer->ChangeGroundModelHundle(-1);
		
	}
	else
	{
		//プレイヤー間が最小の長さの地面番号を
		//送る
		pPlayer->ChangeGroundModelHundle(groundModelHundle_[groundNumber]);
	}

}
