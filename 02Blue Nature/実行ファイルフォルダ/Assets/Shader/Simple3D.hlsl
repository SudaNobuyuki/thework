//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture: register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

Texture2D		g_bump: register(t1);			//テクスチャー
SamplerState	g_bumpsampler : register(s1);	//サンプラー

Texture2D		g_sea: register(t2);			//テクスチャー
SamplerState	g_seasampler : register(s2);	//サンプラー

//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	g_matWVP;			// ワールド・ビュー・プロジェクションの合成行列
	float4x4	g_matNormalTrans;	// 法線の変換行列（回転行列と拡大の逆行列）
	float4x4	g_matWorld;			// ワールド変換行列
	float4		g_vecLightDir;		// ライトの方向ベクトル
	float4		g_vecDiffuse;		// ディフューズカラー（マテリアルの色）
	float4		g_vecAmbient;		// アンビエントカラー（影の色）
	float4		g_vecSpeculer;		// スペキュラーカラー（ハイライトの色）
	float4		g_vecCameraPosition;// 視点（カメラの位置）
	float		g_shuniness;		// ハイライトの強さ（テカリ具合）
	float		g_transparent;		// 透明度
	float		g_time;				// 経過時間	
	bool		g_isTexture;		// テクスチャ貼ってあるかどうか
	bool		g_isBump;			// バンプマッピング用テクスチャがあるかどうか
};

//───────────────────────────────────────
// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
//───────────────────────────────────────
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : TEXCOORD2;		//法線
	float2 uv	  : TEXCOORD0;		//UV座標
	float4 worldPos	  : TEXCOORD1;		//ワールドの位置
};

//───────────────────────────────────────
// 頂点シェーダ
//───────────────────────────────────────
VS_OUT VS(float4 pos : POSITION, float4 Normal : NORMAL, float2 Uv : TEXCOORD)
{
	//ピクセルシェーダーへ渡す情報
	VS_OUT outData;

	//ローカル座標に、ワールド・ビュー・プロジェクション行列をかけて
	//スクリーン座標に変換し、ピクセルシェーダーへ
	outData.pos = mul(pos, g_matWVP);		

	//法線の変形
	Normal.w = 0;					//4次元目は使わないので0
	Normal = mul(Normal, g_matNormalTrans);		//オブジェクトが変形すれば法線も変形
	outData.normal = Normal;		//これをピクセルシェーダーへ

	//視線ベクトル（ハイライトの計算に必要
	outData.worldPos = mul(pos, g_matWorld);					//ローカル座標にワールド行列をかけてワールド座標へ
	
	//UV「座標
	outData.uv = Uv;	//そのままピクセルシェーダーへ


	//まとめて出力
	return outData;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(VS_OUT inData) : SV_Target
{
	//視線ベクトル（ハイライトの計算に必要
	float4 eye = normalize(g_vecCameraPosition - inData.worldPos);	//視点から頂点位置を引き算し視線を求めてピクセルシェーダーへ

	//ライトの向き
	float4 lightDir = g_vecLightDir;	//グルーバル変数は変更できないので、いったんローカル変数へ
	lightDir = normalize(lightDir);	//向きだけが必要なので正規化

	float4 normalColor;					//バンプの法線
	if (g_isBump == true)
	{
		//白黒バンプ
		/*float4 up = g_bump.Sample(g_bumpsampler, inData.uv + float2(0, -1.0f / 256.0f));
		float4 down = g_bump.Sample(g_bumpsampler, inData.uv + float2(0, 1.0f / 256.0f));
		float4 right = g_bump.Sample(g_bumpsampler, inData.uv + float2(1.0f / 256.0f, 0));
		float4 left = g_bump.Sample(g_bumpsampler, inData.uv + float2(-1.0f / 256.0f, 0));

		float3 xpoint = float3(1, 0, (right[0] - left[0]) / 2);
		float3 ypoint = float3(0, 1, (up[0] - down[0]) / 2);

		normalColor = float4(cross(ypoint, xpoint), 0);
		normalColor = mul(normalColor, g_matNormalTrans);
		normalColor = normalize(normalColor);*/

		//rgbバンプ
		normalColor = g_bump.Sample(g_bumpsampler, inData.uv);
		normalColor = 2.0f * normalColor - 1.0f;
		normalColor.w = 0;
		normalColor = mul(normalColor, g_matNormalTrans);
		normalColor = float4(normalColor.x, -normalColor.z, -normalColor.y, normalColor.w);		// maya
		//normalColor = float4(normalColor.x, normalColor.y, normalColor.z, normalColor.w);		// blender
	}
	else
	{
		//fbxで設定されていた法線をセット
		normalColor = inData.normal;
	}

	//法線はピクセルシェーダーに持ってきた時点で補完され長さが変わっている
	//正規化しておかないと面の明るさがおかしくなる
	normalColor = normalize(normalColor);

	//拡散反射光（ディフューズ）
	//法線と光のベクトルの内積が、そこの明るさになる
	float4 shade = saturate(dot(normalColor, -lightDir));
	shade.a = 1;	//暗いところが透明になるので、強制的にアルファは1

	float4 diffuse;
	//テクスチャ有無
	if (g_isTexture == true)
	{
		//テクスチャの色
		diffuse = g_texture.Sample(g_sampler, inData.uv);
	}
	else
	{
		//マテリアルの色
		diffuse = g_vecDiffuse;
	}
	diffuse.a *= g_transparent;

	//環境光（アンビエント）
	//これはMaya側で指定し、グローバル変数で受け取ったものをそのまま
	float4 ambient = g_vecAmbient + 0.5f;

	//鏡面反射光（スペキュラー）
	float4 speculer = float4(0, 0, 0, 0);	//とりあえずハイライトは無しにしておいて…
	if (g_vecSpeculer.a != 0)	//スペキュラーの情報があれば
	{
		float4 R = reflect(lightDir, normalColor);			//正反射ベクトル
		speculer = pow(saturate(dot(R, eye)), g_shuniness) * g_vecSpeculer * 0.5f;	//ハイライトを求める
	}

	float4 refVec = float4(0, -1.0f, 0, 0);
	refVec = saturate(dot(normalColor, -refVec));

	inData.worldPos.z += g_time * 1.5f;

	float4 seaColor = g_sea.Sample(g_seasampler, inData.worldPos.xz / 100.0f);
	seaColor *= refVec;
	seaColor /= (200.0f - inData.worldPos.y) / 400.0f;

	//最終的な色
	return diffuse * shade + diffuse * ambient + speculer + diffuse * seaColor;
}