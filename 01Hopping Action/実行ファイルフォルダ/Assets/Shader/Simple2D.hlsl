//───────────────────────────────────────
// テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

//───────────────────────────────────────
// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	//ワールド行列
	float4x4	WorldMatrix;

	//透明度
	float		ALPHA;
};

//───────────────────────────────────────
// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
//───────────────────────────────────────
struct VS_OUT
{
	//位置
	float4 pos : SV_POSITION;

	//UV座標
	float2 uv : TEXCOORD;
};

//───────────────────────────────────────
// 頂点シェーダ
//───────────────────────────────────────
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{
	//ピクセルシェーダーへ渡す情報
	VS_OUT outData;

	//スクリーン座標に変換し、ピクセルシェーダーへ
	outData.pos = mul(pos, WorldMatrix);
	outData.uv = float2(uv.x, uv.y);

	//まとめて出力
	return outData;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(VS_OUT inData) : SV_Target
{
	//画像の色
	float4 color = g_texture.Sample(g_sampler, inData.uv);

	//アルファ値設定
	color.a *= ALPHA;

	//画像の色を返す
	return color;
}
