//───────────────────────────────────────
// テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

//───────────────────────────────────────
// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	//ワールド・ビュー・プロジェクションの合成行列
	float4x4	WVPmatrix;

	//ディフューズカラー(マテリアルの色)
	float4		diffuseColor;

	//テクスチャ貼ってあるかどうか
	bool		isTexture;
};

//頂点シェーダの出力結果
struct VS_OUT
{
	//座標
	float4 pos : SV_POSITION;

	//UV座標
	float2 uv : TEXCOORD;
};

//頂点シェーダ
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL)
{
	//出力結果
	VS_OUT outData;

	//座標変更
	outData.pos = mul(pos, WVPmatrix);
	
	//座標
	outData.uv = float2(uv.x, uv.y);

	//出力
	return outData;
}

//ピクセルシェーダ
float4 PS(VS_OUT inData) : SV_TARGET
{
	//テクスチャありの色
	if (isTexture)
		return g_texture.Sample(g_sampler, inData.uv);
	
	//テクスチャなしの色
	else
		return diffuseColor;
}