//インクルードガード
#pragma once
#ifndef RESULT_H
#define RESULT_H
#endif // !RESULT_H


//インクルード
#include "Engine/GameObject.h"


//リザルトを管理するクラス
class Result : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Result(GameObject* parent);

	//デストラクタ
	//param :なし
	~Result();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*リザルト画像管理*/

	//リザルト画像の種類
	enum RESULT_IMAGES
	{
		BACKGROUND,
		TIME,
		RECORD,
		NEW_RECORD,
		RESULT_IMAGE_MAXIMUM
	};

	//リザルト画像ID
	int resultImageID_[RESULT_IMAGE_MAXIMUM];

	//リザルト画像の描画時間の計測
	int resultImageDrawTime_;

	//各リザルト画像の初期設定
	//param :なし
	//return:なし
	void ResultImageInitialSetting();

	//各リザルト画像のアルファ値を設定する
	//param :なし
	//return:なし
	void ResultImageProcessSettingAlpha();
};