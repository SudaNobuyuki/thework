//インクルード
#include "PlayScene.h"
#include "Pause.h"
#include "Stage.h"
#include "Player.h"
#include "Result.h"
#include "Timer.h"


//定数
//Initioalize()
static const float CLEAR_COLOR[RGB] = { 0.2f, 0.4f, 0.8f };		//このシーンの背景色


//コンストラクタ
PlayScene::PlayScene(GameObject * parent) : GameObject(parent, "PlayScene")
{
}

//デストラクタ
PlayScene::~PlayScene()
{
	//各オブジェクトのポインタの初期化
	ObjectsRelease();
}

//初期化
void PlayScene::Initialize()
{
	//背景色変更
	Direct3D::SetClearColor(CLEAR_COLOR);

	//各オブジェクトのポインタの初期化
	ObjectsInitialize();

	//ステージオブジェクトの生成
	StageGeneration();

	//プレイヤーオブジェクトの生成
	PlayerGeneration();

	//ポーズオブジェクトの生成
	PauseGeneration();

	//リザルトオブジェクトの生成
	ResultGeneration();
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

//各オブジェクトのポインタの解放
void PlayScene::ObjectsRelease()
{
	//ステージオブジェクト
	pStage_ = nullptr;

	//プレイヤーオブジェクト
	pPlayer_ = nullptr;

	//タイマーオブジェクト
	pTimer_ = nullptr;

	//ポーズオブジェクト
	pPause_ = nullptr;

	//リザルトオブジェクト
	pResult_ = nullptr;
}

//ステージオブジェクトの生成
void PlayScene::StageGeneration()
{
	//ステージオブジェクトの生成
	pStage_ = Instantiate<Stage>();

	//生成に成功したか
	assert(pStage_ != nullptr);
}

//プレイヤーオブジェクトの生成
void PlayScene::PlayerGeneration()
{
	//プレイヤーオブジェクトの生成
	pPlayer_ = Instantiate<Player>();

	//生成に成功したか
	assert(pPlayer_ != nullptr);
}

//タイマーオブジェクトの生成
void PlayScene::TimerGeneration()
{
	//タイマーオブジェクトの生成
	pTimer_ = Instantiate<Timer>();

	//生成に成功したか
	assert(pTimer_ != nullptr);
}

//ポーズオブジェクトの生成
void PlayScene::PauseGeneration()
{
	//ポーズオブジェクトの生成
	pPause_ = Instantiate<Pause>();

	//生成に成功したか
	assert(pPause_ != nullptr);
}

//リザルトオブジェクトの生成
void PlayScene::ResultGeneration()
{
	//リザルトオブジェクトの生成
	pResult_ = Instantiate<Result>();

	//生成に成功したか
	assert(pResult_ != nullptr);
}

//NewRecordか
bool PlayScene::IsNewRecord()
{
	//NewRecordか
	return dynamic_cast<Timer*>(pTimer_)->IsNewRecord();
}

//そこは壁か調べる
bool PlayScene::IsWall(XMVECTOR & position)
{
	//そこは壁か調べる
	return dynamic_cast<Stage*>(pStage_)->IsWall(position);
}

//そこがゴールか調べる(ゴール時にはゴールしたと記憶)
bool PlayScene::IsGoal(XMVECTOR & position)
{
	//そこがゴールか調べる(ゴール時にはゴールしたと記憶)
	return isGoal_ = dynamic_cast<Stage*>(pStage_)->IsGoal(position);
}