//インクルード
#include "Splash.h"
#include "StartScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//定数
//SplashImageFadeProcess()
static const int FADE_IN_START   = 0;			//フェードイン開始フレーム
static const int FADE_IN_END     = 125;			//フェードイン終了フレーム
static const int FADE_OUT_START  = 250;			//フェードアウト開始フレーム
static const int FADE_OUT_END    = 400;			//フェードアウト終了フレーム

//TitleObjectInstantiateProcess()
static const int SLEEP_TIME      = 2000;		//タイトルオブジェクトの生成までの間


//マクロ
//Update()
#define ALPHA_CALCULATING_IN_FADE_IN (splashImageFadeInterval_ * 0.008f)						//フェードインで使われるα値
#define ALPHA_CALCULATING_IN_FADE_OUT ((FADE_OUT_END - splashImageFadeInterval_) * 0.008f)		//フェードアウトで使われるα値


//コンストラクタ
Splash::Splash(GameObject * parent) : GameObject(parent, "Splash"),
	splashImageID_(INITIAL_ID), splashImageFadeInterval_(0), splashImageAlpha_(0)
{
}

//デストラクタ
Splash::~Splash()
{
}

//初期化
void Splash::Initialize()
{
	//スプラッシュ画像の初期設定
	SplashImageInitialSetting();
}

//更新
void Splash::Update()
{
	//スプラッシュ画像のフェード処理
	SplashImageFadeProcess();
}

//描画
void Splash::Draw()
{
	//スプラッシュ画像の描画
	Image::Draw(splashImageID_);
}

//開放
void Splash::Release()
{
}

//スプラッシュ画像の初期設定
void Splash::SplashImageInitialSetting()
{
	//スプラッシュ画像データのロード
	splashImageID_ = Image::Load("Assets/StartScene/Splash.png");
	assert(splashImageID_ >= 0);

	//スプラッシュ画像の拡大率をウィンドウサイズに調整
	Image::SetWindowSize(splashImageID_);
}

//スプラッシュ画像のフェード処理
void Splash::SplashImageFadeProcess()
{
	//スプラッシュ画像のフェード間隔を進める処理
	splashImageFadeInterval_++;

	//フェードイン処理(splashImageFadeInterval_が[0〜125]フレームの場合)
	if (splashImageFadeInterval_ >= FADE_IN_START && splashImageFadeInterval_ <= FADE_IN_END)
		splashImageAlpha_ = ALPHA_CALCULATING_IN_FADE_IN;

	//フェードアウト処理(splashImageFadeInterval_が[250〜400]フレームの場合)
	else if (splashImageFadeInterval_ >= FADE_OUT_START && splashImageFadeInterval_ <= FADE_OUT_END)
		splashImageAlpha_ = ALPHA_CALCULATING_IN_FADE_OUT;

	//フェードアウト終了後タイトルを表示し、自信を殺す処理(splashImageFadeInterval_が[401フレーム〜]の場合)
	else if(splashImageFadeInterval_ > FADE_OUT_END)
		TitleObjectInstantiateProcess();

	//スプラッシュ画像のα値を渡す
	Image::SetAlpha(splashImageID_, splashImageAlpha_);
}

//タイトルオブジェクトを生成し、自信を殺す処理
void Splash::TitleObjectInstantiateProcess()
{
	//少し停止
	Sleep(SLEEP_TIME);

	//タイトルオブジェクトの生成
	dynamic_cast<StartScene*>(pParent_)->TitleGeneration();

	//自身を殺す
	KillMe();
}