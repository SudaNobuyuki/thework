﻿//インクルードガード
#pragma once
#ifndef FBX_H
#define FBX_H
#endif //!FBX_H


//インクルード
#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include "Transform.h"
#include "Direct3D.h"


//コメント
#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")


//プロトタイプ宣言
class Texture;


//モデルを管理するクラス
class Fbx
{
public:
	/*コンストラクタ、デストラクタ*/

	//コンストラクタ
	Fbx();

	//デストラクタ
	~Fbx();

protected:
	/*シェーダーに渡す情報の管理*/

	//マテリアル
	struct MATERIAL
	{
		Texture* pTexture;		//テクスチャ情報
		XMFLOAT4 diffuse;		//ディフューズ
	};

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX WVPMatrix;			//ワールド・ビュー・プロジェクションの合成行列
		XMFLOAT4 diffuseColor;		//ディフューズカラー(マテリアルの色)
		BOOL isTexture;				//テクスチャ貼ってあるかどうか
	};
	
	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;		//座標
		XMVECTOR uv;			//uv
	};

	//頂点数
	int vertexCount_;

	//ポリゴン数
	int polygonCount_;

	//マテリアルの個数
	int materialCount_;

	//マテリアル毎のインデックス数
	int* pIndexCountEachMaterial_;

	//インデックスデータ
	int** ppIndex_;

	//頂点バッファ
	ID3D11Buffer* pVertexBuffer_;

	//インデックスバッファ
	ID3D11Buffer** ppIndexBuffer_;

	//コンスタントバッファ
	ID3D11Buffer* pConstantBuffer_;

	//マテリアル情報
	MATERIAL* pMaterialList_;

	//頂点データ
	VERTEX* pVertices_;

	//頂点バッファ準備
	//引数:メッシュ情報
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitializeVertex(fbxsdk::FbxMesh* mesh);
	
	//インデックスバッファ準備
	//引数:メッシュ情報
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitializeIndex(fbxsdk::FbxMesh* mesh);

	//コンスタントバッファ準備
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitializeConstantBuffer();

	//マテリアル準備
	//引数:ノード情報
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitializeMaterial(fbxsdk::FbxNode* pNode);

public:
	/*モデルの管理*/

	//読み込み
	//引数:ファイルパス
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Load(std::string fileName);

	//描画
	//引数:ワールド座標(Transform&)
	//戻値:なし
	HRESULT Draw(Transform& transform);

	//解放
	//引数:なし
	//戻値:なし
	void Release();
};

