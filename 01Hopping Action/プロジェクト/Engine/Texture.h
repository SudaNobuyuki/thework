//インクルードガード
#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H
#endif //!TEXTURE_H


//インクルード
#include <d3d11.h>
#include <string>


//モデルのテクスチャを管理するクラス
class Texture
{
public:
	/*コンストラクタ、デストラクタ*/

	//コンストラクタ
	Texture();

	//デストラクタ
	~Texture();

private:
	/*テクスチャデータを管理*/

	//サンプラー
	ID3D11SamplerState* pSampler_;

	//シェーダーリソースビュー
	ID3D11ShaderResourceView* pShaderResourceView_;

	//テクスチャサイズ
	UINT width_;
	UINT height_;

public:
	/*テクスチャの情報管理*/

	//画像の読み込み
	//引数:ファイル名
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Load(std::string fileName);

	//Textureの解放
	//引数:なし
	//戻値:なし
	void Release();

	//サンプラーの取得
	//引数:なし
	//戻値:サンプラー
	ID3D11SamplerState* GetSampler() { return pSampler_; }

	//シェードリソースビューの取得
	//引数:なし
	//戻値:シェードリソースビュー
	ID3D11ShaderResourceView* GetSRV() { return pShaderResourceView_; }

	//画像サイズの取得
	//引数:なし
	//戻値:サイズ
	UINT GetWidth() { return width_; };
	UINT GetHeight() { return height_; };
};