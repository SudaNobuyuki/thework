//インクルード
#include "SceneManager.h"
#include "Model.h"
#include "Image.h"
#include "../StartScene.h"
#include "../StageSelectScene.h"
#include "../PlayScene.h"


//コンストラクタ
SceneManager::SceneManager()
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = nextSceneID_ = SCENE_ID_START;
	Instantiate<StartScene>();
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う ＝ シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//オブジェクトの解放
		AllChildKill();

		//ロードしたデータを全削除
		Model::Release();
		Image::Release();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_START: Instantiate<StartScene>(); break;
		case SCENE_ID_SELECT: Instantiate<StageSelectScene>(); break;
		case SCENE_ID_PLAY: Instantiate<PlayScene>(); break;
		}

		//シーン記憶
		currentSceneID_ = nextSceneID_;
	}

	//画像の奥行をリセット
	Image::ResetDepth();
}

//描画
void SceneManager::Draw()
{
}

//解放
void SceneManager::Release()
{
}

//次のシーンへ移動(存在しない場合には一番最初のシーンへ戻る)
void SceneManager::NextScene()
{
	//次のシーン
	nextSceneID_ = (SCENE_ID)(currentSceneID_ + 1);

	//もし次のシーンがないなら最初に戻す
	if (nextSceneID_ == SCENE_MAX)
		nextSceneID_ = (SCENE_ID)0;
}