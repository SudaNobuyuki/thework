//インクルードガード
#pragma once
#ifndef IMAGE_H
#define IMAGE_H
#endif //!IMAGE_H


//インクルード
#include <vector>
#include "Sprite.h"


//画像を管理する名前空間
namespace Image
{
	//1画像の情報
	struct ImageData
	{
		Transform transform_;		//トランスフォーム
		std::string filePath_;		//モデルパス
		Sprite* pSprite_;			//画像
	};

	//画像をロード
	//引数:読み込む画像のパス
	//戻値:正常に画像を読み込めたら0以上、読み込めなかったら-1
	int Load(std::string ImageName);

	//画像の表示位置のみ設定
	//引数:表示させるモデルの番号
	//引数:表示させるtransform
	//戻値:なし
	void SetPosition(int imageHandle, Transform& transform);

	//画像の表示位置と拡大率設定
	//引数:表示させるモデルの番号
	//引数:表示させるtransform
	//戻値:なし
	void SetPositionAndScale(int imageHandle, Transform& transform);

	//画像の表示設定
	//引数:表示させるモデルの番号
	//引数:表示させるtransform
	//戻値:なし
	void SetTransform(int imageHandle, Transform& transform);

	//画像を描画
	//引数:表示させるモデルの番号
	//戻値:なし
	void Draw(int imageHandle);

	//画像すべて解放
	//引数:なし
	//戻値:なし
	void Release();

	//α値セット
	//引数:表示させるモデルの番号
	//引数:α値
	//戻値:なし
	void SetAlpha(int imageHandle, float a);

	//α値リセット
	//引数:表示させるモデルの番号
	//戻値:なし
	void ResetAlpha(int imageHandle);

	//画像の拡大率をウィンドウサイズに調整
	//引数:画像ID
	//引数:ウィンドウサイズに合わせた後に掛ける最終的な拡大率
	//戻値:サイズ
	XMVECTOR SetWindowSize(int imageHandle, float scale = 1.0f);

	//画像の奥行をリセット
	//引数:なし
	//戻値:なし
	void ResetDepth();
}