//インクルードガード
#pragma once
#ifndef INPUT_H
#define INPUT_H
#endif //!INPUT_H


//マクロ
#define DIRECTINPUT_VERSION 0x0800


//インクルード
#include <dInput.h>
#include <DirectXMath.h>
#include <string>
#include "Direct3D.h"
#include "XInput.h"


//コメント
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib, "Xinput.lib")


//マクロ
#define MOUSE_LEFT  0x00
#define MOUSE_RIGHT 0x01
#define MOUSE_WHEEL 0x02


//名前空間
using namespace DirectX;


//入力を管理する名前空間
namespace Input
{
	//初期化
	//引数:ウィンドウハンドル
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Initialize(HWND hwnd);

	//更新
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Update();
	
	//解放
	//引数:なし
	//戻値:なし
	void Release();

	/*****************
		キーボード
	*****************/
	//キーボードの入力チェック
	//引数:チェック入力キーコード
	//戻値:押しているかどうか
	bool IsKey(int keyCode);
	
	//キーボードの入力チェック(押した瞬間)
	//引数:チェック入力キーコード
	//戻値:押しているかどうか
	bool IsKeyDown(int keyCode);

	//2キーのどちらの入力されたかチェック(倍率なし)
	//引数:プラスキー
	//引数:マイナスキー
	//戻値:(プラスキーの結果 - マイナスキーの結果)
	float KeyStateCalculation(int plus, int minus);

	//2キーのどちらの入力されたかチェック(倍率あり)
	//引数:プラスキー
	//引数:マイナスキー
	//引数:倍率
	//戻値:(プラスキーの結果 - マイナスキーの結果) * 倍率
	float KeyStateCalculation(int plus, int minus, float magnification);

	//パラメータパックが空になったら終了
	//引数:なし
	//戻値:false
	bool KeyStateCheck();

	//複数キー中のどれか入力されたか
	//引数:今調べたいキー
	//引数:これから調べるキー
	//戻値:どれかでも押されていたらtrue
	template<class Head, class... Tail>
	bool KeyStateCheck(Head&& head, Tail&&... tail)
	{
		//キーチェック
		bool isKey = IsKey(head);

		//パラメータパックtailをさらにheadとtailに分割
		isKey |= KeyStateCheck(std::forward<Tail>(tail)...);

		//結果を返却
		return isKey;
	}

	//パラメータパックが空になったら終了
	//引数:なし
	//戻値:false
	bool KeyStateCheckDown();

	//複数キー中のどれか入力されたか
	//引数:今調べたいキー
	//引数:これから調べるキー
	//戻値:どれかでも押されていたらtrue
	template<class Head, class... Tail>
	bool KeyStateCheckDown(Head&& head, Tail&&... tail)
	{
		//キーチェック
		bool isKey = IsKeyDown(head);

		//パラメータパックtailをさらにheadとtailに分割
		isKey |= KeyStateCheckDown(std::forward<Tail>(tail)...);

		//結果を返却
		return isKey;
	}

	/*************
		マウス
	*************/
	//マウスの入力チェック
	//引数:チェック入力マウスコード
	//戻値:押しているかどうか
	bool IsMouseButton(int buttonCode);

	//マウスの入力チェック(押した瞬間)
	//引数:チェック入力マウスコード
	//戻値:押しているかどうか
	bool IsMouseButtonDown(int buttonCode);

	//マウスの入力チェック(離した瞬間)
	//引数:チェック入力マウスコード
	//戻値:押しているかどうか
	bool IsMouseButtonUp(int buttonCode);

	//ウィンドウ上のマウスカーソルの位置取得
	//引数:なし
	//戻値:位置
	XMVECTOR GetMousePosition();

	//マウスの位置を設定
	//引数:位置
	//戻値:なし
	void SetMousePosition(int x, int y);

	//そのフレームでのマウスの移動量を取得(X)
	//引数:なし
	//戻値:移動量
	float GetMouseMoveX();

	//そのフレームでのマウスの移動量を取得(Y)
	//引数:なし
	//戻値:移動量
	float GetMouseMoveY();

	//そのフレームでのマウスの移動量を取得(ホイール)
	//引数:なし
	//戻値:移動量
	int GetMouseMoveWheel();

	//そのフレームでのマウスの移動量を取得(ALL)
	//引数:なし
	//戻値:移動量
	XMVECTOR GetMouseMove();
};