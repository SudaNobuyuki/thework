//インクルードガード
#pragma once
#ifndef SPRITE_H
#define SPRITE_H
#endif //!SPRITE_H


//インクルード
#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"


//名前空間
using namespace DirectX;


//画像を管理するクラス
class Sprite
{
public:
	/*コンストラクタ、デストラクタ*/

	//コンストラクタ
	Sprite();

	//デストラクタ
	~Sprite();

protected:
	/*シェーダーに渡す情報の管理*/

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX worldMatrix;		//ワールド行列
		float alpha;				//透明度
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;		//座標
		XMVECTOR uv;			//uv
	};

	//設定するアルファ値
	float alpha_;

	//頂点バッファ
	ID3D11Buffer* pVertexBuffer_;

	//インデックスバッファ
	ID3D11Buffer* pIndexBuffer_;

	//コンスタントバッファ
	ID3D11Buffer* pConstantBuffer_;

	//テクスチャクラスのアドレス
	Texture* pTexture_;

	//インデックス情報の数
	int indexCount_;

public:
	/*画像の管理*/

	//初期化
	//引数:ファイルパス
	//戻値:成否判定の結果(S_OK || E_FAIL)
	virtual HRESULT Initialize(std::string fileName);

	//描画
	//引数:ワールド座標(Transform&)
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Draw(Transform& transform);

	//解放
	//引数:なし
	//戻値:なし
	void Release();

	//アルファ値セット
	//引数:アルファ値
	//戻値:なし
	void SetAlpha(float a) { alpha_ = a; };

	//画像サイズ取得
	//引数:なし
	//戻値:画像サイズ
	UINT GetSizeX() { return pTexture_->GetWidth(); };
	UINT GetSizeY() { return pTexture_->GetHeight(); };
};

