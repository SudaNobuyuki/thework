﻿//インクルードガード
#pragma once
#ifndef DIRECT3D_H
#define DIRECT3D_H
#endif //!DIRECT3D_H


//インクルード
#include <d3d11.h>
#include <assert.h>


//コメント
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")


//定数
static const int R = 0;			//赤
static const int G = 1;			//緑
static const int B = 2;			//青
static const int RGB = 3;		//背景色等


//マクロ
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}												//Deleteしていなければする
#define SAFE_ARRAY_DELETE(p) if(p != nullptr){ delete[] p; p = nullptr;}										//Deleteしていなければする
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}											//Releaseしていなければする
#define CHECK_HRESULT(hr, str) if(FAILED(hr)){ MessageBox(nullptr, str, "エラー", MB_OK); return E_FAIL;}		//HRESULT型に正しい値が入ったかどうか


//シェーダータイプ
enum SHADER_TYPE
{
	SHADER_3D = 0,
	SHADER_2D,
	SHADER_MAX
};


//DirectXを用いるゲームエンジン全体を管理する名前空間
namespace Direct3D
{
	//デバイス
	extern 	ID3D11Device* pDevice_;
	extern  ID3D11DeviceContext* pContext_;

	//ウィンドウサイズ
	extern int screenWidth_;
	extern int screenHeight_;

	//ウィンドウハンドル
	extern HWND hwnd_;

	//初期化
	//引数:ウィンドウ幅(int)
	//引数:ウィンドウ高さ(int)
	//引数:ウィンドウハンドル(HWND)
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT Initialize(int screenWidth, int screenHeight, HWND hwnd);

	//シェーダー準備
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitShader();

	//3Dシェーダーのコンパイル
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitShader3D();

	//2Dシェーダーのコンパイル
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT InitShader2D();

	//2Dまたは3Dをデバイスコンテキストにセット
	//引数:SHADER_TYPEのどれか
	//戻値:なし
	void SetShaderBundle(SHADER_TYPE type);

	//描画開始
	//引数:なし
	//戻値:なし
	void BeginDraw();

	//描画終了
	//引数:なし
	//戻値:成否判定の結果(S_OK || E_FAIL)
	HRESULT EndDraw();

	//解放
	//引数:なし
	//戻値:なし
	void Release();

	//画像描画時、Zバッファ無効
	//引数:画像かどうか
	//戻値:なし
	void SetTarget(bool isPict);

	//背景色の設定
	//引数:色(RGB)
	//戻値:なし
	void SetClearColor(float color[RGB]);
	void SetClearColor(const float color[RGB]);

	//背景色の取得
	//引数:入れたい配列
	//戻値:なし
	void GetClearColor(float color[RGB]);

	//ウィンドウの中心を取得
	//引数:なし
	//戻値:中央
	int GetWindowCenterX();
	int GetWindowCenterY();
};