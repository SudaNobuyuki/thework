﻿//インクルード
#include <d3dcompiler.h>
#include <d3dcompiler.h>
#include "Direct3D.h"
#include "Camera.h"
#include "Texture.h"


//DirectXを用いるゲームエンジン全体を管理する名前空間
namespace Direct3D
{
	//エンジン
	ID3D11Device* pDevice_ = nullptr;							//デバイス
	ID3D11DeviceContext* pContext_ = nullptr;					//デバイスコンテキスト
	IDXGISwapChain* pSwapChain_ = nullptr;						//スワップチェイン
	ID3D11RenderTargetView* pRenderTargetView_ = nullptr;		//レンダーターゲットビュー
	ID3D11Texture2D* pDepthStencil_ = nullptr;					//深度ステンシル
	ID3D11DepthStencilView* pDepthStencilView_ = nullptr;		//深度ステンシルビュー

	//スクリーンサイズ
	int screenWidth_ = 0;
	int screenHeight_ = 0;

	//シェーダの設定
	struct ShaderBundle
	{
		ID3D11VertexShader* pVertexShader_ = nullptr;			//頂点シェーダー
		ID3D11PixelShader* pPixelShader_ = nullptr;				//ピクセルシェーダー
		ID3D11InputLayout* pVertexLayout_ = nullptr;			//頂点インプットレイアウト
		ID3D11RasterizerState* pRasterizerState_ = nullptr;		//ラスタライザー
	};
	ShaderBundle shaderBundle_[SHADER_MAX];						//[0] = 3D, [1] = 2D

	//深度
	ID3D11BlendState* pBlendState;

	//画面の色設定
	float clearColor_[4] = { 0, 0, 0, 1.0f };//R,G,B,A;

	//ウィンドウハンドル
	HWND hwnd_;
}

//初期化
HRESULT Direct3D::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
	//ハンドル設定
	hwnd_ = hwnd;

	///////////////////////////いろいろ準備するための設定///////////////////////////////
	//いろいろな設定項目をまとめた構造体
	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	//とりあえず全部0
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	//描画先のフォーマット
	swapChainDesc.BufferDesc.Width = screenWidth;						//画面幅(バックバッファの画用紙サイズ)
	swapChainDesc.BufferDesc.Height = screenHeight;						//画面高さ(バックバッファの画用紙サイズ)
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;		//何色使えるか

	//FPS（1/60秒に1回）
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	//その他
	swapChainDesc.Windowed = TRUE;										//ウィンドウモードかフルスクリーンか
	swapChainDesc.OutputWindow = hwnd_;									//ウィンドウハンドル
	swapChainDesc.BufferCount = 1;										//バックバッファの枚数
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;		//バックバッファの使い道＝画面に描画するために
	swapChainDesc.SampleDesc.Count = 1;									//MSAA(アンチエイリアス)の設定
	swapChainDesc.SampleDesc.Quality = 0;								//MSAA(アンチエイリアス)の設定

	////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成////////////////////////
	D3D_FEATURE_LEVEL level;
	CHECK_HRESULT(D3D11CreateDeviceAndSwapChain(
		nullptr,						//どのビデオアダプタを使用するか？既定ならばnullptrで
		D3D_DRIVER_TYPE_HARDWARE,		//ドライバのタイプを渡す。ふつうはHARDWARE
		nullptr,						//上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
		0,								//何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
		nullptr,						//デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
		0,								//上の引数でレベルを何個指定したか
		D3D11_SDK_VERSION,				//SDKのバージョン。必ずこの値
		&swapChainDesc,					//上でいろいろ設定した構造体
		&pSwapChain_,					//無事完成したSwapChainのアドレスが返ってくる
		&pDevice_,						//無事完成したDeviceアドレスが返ってくる
		&level,							//無事完成したDevice、Contextのレベルが返ってくる
		&pContext_)						//無事完成したContextのアドレスが返ってくる
		, "デバイス、コンテキスト、スワップチェインの作成に失敗しました");

	///////////////////////////レンダーターゲットビュー作成///////////////////////////////
	//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
	ID3D11Texture2D* pBackBuffer;
	CHECK_HRESULT(pSwapChain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer), "スワップチェーンからバックバッファを取得に失敗しました");

	//レンダーターゲットビューを作成
	CHECK_HRESULT(pDevice_->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView_), "レンダーターゲットビューの作成に失敗しました");

	//一時的にバックバッファを取得しただけなので解放
	SAFE_RELEASE(pBackBuffer);

	///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
	//レンダリング結果を表示する範囲
	D3D11_VIEWPORT viewport;
	viewport.Width = (float)screenWidth;		//幅
	viewport.Height = (float)screenHeight;		//高さ
	viewport.MinDepth = 0.0f;					//手前
	viewport.MaxDepth = 1.0f;					//奥
	viewport.TopLeftX = 0;						//左
	viewport.TopLeftY = 0;						//上

	//深度ステンシルビューの作成
	D3D11_TEXTURE2D_DESC depthDesc;
	depthDesc.Width = screenWidth;
	depthDesc.Height = screenHeight;
	depthDesc.MipLevels = 1;
	depthDesc.ArraySize = 1;
	depthDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthDesc.SampleDesc.Count = 1;
	depthDesc.SampleDesc.Quality = 0;
	depthDesc.Usage = D3D11_USAGE_DEFAULT;
	depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthDesc.CPUAccessFlags = 0;
	depthDesc.MiscFlags = 0;
	pDevice_->CreateTexture2D(&depthDesc, NULL, &pDepthStencil_);
	pDevice_->CreateDepthStencilView(pDepthStencil_, NULL, &pDepthStencilView_);

	//データを画面に描画するための一通りの設定（パイプライン）
	pContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);		//データの入力種類を指定
	pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView_);		//描画先を設定
	pContext_->RSSetViewports(1, &viewport);

	//シェーダー準備
	CHECK_HRESULT(InitShader(), "シェーダー準備に失敗しました");

	//カメラの初期化
	Camera::Initialize();

	//ウィンドウサイズを記憶
	screenWidth_ = screenWidth;
	screenHeight_ = screenHeight;

	//ブレンドステート
	D3D11_BLEND_DESC BlendDesc;
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	pDevice_->CreateBlendState(&BlendDesc, &pBlendState);
	float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
	pContext_->OMSetBlendState(pBlendState, blendFactor, 0xffffffff);

	//成功
	return S_OK;
}

//シェーダー準備
HRESULT Direct3D::InitShader()
{
	//3Dシェーダのコンパイル
	CHECK_HRESULT(InitShader3D(), "3Dシェーダのコンパイルに失敗しました");

	//2Dシェーダのコンパイル
	CHECK_HRESULT(InitShader2D(), "2Dシェーダのコンパイルに失敗しました");

	//2Dまたは3Dをデバイスコンテキストにセット
	SetShaderBundle(SHADER_3D);

	//成功
	return S_OK;
}

//3Dシェーダのコンパイル
HRESULT Direct3D::InitShader3D()
{
	//頂点シェーダの作成（コンパイル）
	ID3DBlob* pCompileVS = nullptr;
	D3DCompileFromFile(L"Assets/Shader/Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	assert(pCompileVS != nullptr);
	CHECK_HRESULT(pDevice_->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle_[SHADER_3D].pVertexShader_), "頂点シェーダの作成に失敗しました");

	//頂点インプットレイアウト
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },						//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },			//UV座標
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 , D3D11_INPUT_PER_VERTEX_DATA, 0 },		//法線
	};
	CHECK_HRESULT(pDevice_->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle_[SHADER_3D].pVertexLayout_), "頂点インプットレイアウトに失敗しました");
	SAFE_RELEASE(pCompileVS);

	//ピクセルシェーダの作成（コンパイル）
	ID3DBlob* pCompilePS = nullptr;
	D3DCompileFromFile(L"Assets/Shader/Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	assert(pCompilePS != nullptr);
	CHECK_HRESULT(pDevice_->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle_[SHADER_3D].pPixelShader_), "ピクセルシェーダの作成に失敗しました");
	SAFE_RELEASE(pCompilePS);

	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rasterizer = {};
	rasterizer.CullMode = D3D11_CULL_BACK;
	rasterizer.FillMode = D3D11_FILL_SOLID;
	rasterizer.FrontCounterClockwise = FALSE;
	CHECK_HRESULT(pDevice_->CreateRasterizerState(&rasterizer, &shaderBundle_[SHADER_3D].pRasterizerState_), "ラスタライザの作成に失敗しました");

	//成功
	return S_OK;
}

//2Dシェーダのコンパイル
HRESULT Direct3D::InitShader2D()
{
	//頂点シェーダの作成（コンパイル）
	ID3DBlob* pCompileVS = nullptr;
	D3DCompileFromFile(L"Assets/Shader/Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	assert(pCompileVS != nullptr);
	CHECK_HRESULT(pDevice_->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle_[SHADER_2D].pVertexShader_), "頂点シェーダの作成に失敗しました");

	//頂点インプットレイアウト
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },					//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },		//UV座標
	};
	CHECK_HRESULT(pDevice_->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle_[SHADER_2D].pVertexLayout_), "頂点インプットレイアウトに失敗しました");
	SAFE_RELEASE(pCompileVS);

	//ピクセルシェーダの作成（コンパイル）
	ID3DBlob* pCompilePS = nullptr;
	D3DCompileFromFile(L"Assets/Shader/Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	assert(pCompilePS != nullptr);
	CHECK_HRESULT(pDevice_->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle_[SHADER_2D].pPixelShader_), "ピクセルシェーダの作成に失敗しました");
	SAFE_RELEASE(pCompilePS);

	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rasterizer = {};
	rasterizer.CullMode = D3D11_CULL_BACK;
	rasterizer.FillMode = D3D11_FILL_SOLID;
	rasterizer.FrontCounterClockwise = FALSE;
	CHECK_HRESULT(pDevice_->CreateRasterizerState(&rasterizer, &shaderBundle_[SHADER_2D].pRasterizerState_), "ラスタライザの作成に失敗しました");

	//成功
	return S_OK;
}

//それぞれをデバイスコンテキストにセット
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	pContext_->VSSetShader(shaderBundle_[type].pVertexShader_, NULL, 0);		//頂点シェーダー
	pContext_->PSSetShader(shaderBundle_[type].pPixelShader_, NULL, 0);			//ピクセルシェーダー
	pContext_->IASetInputLayout(shaderBundle_[type].pVertexLayout_);			//頂点インプットレイアウト
	pContext_->RSSetState(shaderBundle_[type].pRasterizerState_);				//ラスタライザー
}

//描画開始
void Direct3D::BeginDraw()
{
	//カメラの更新
	Camera::Update();

	//画面をクリア
	pContext_->ClearRenderTargetView(pRenderTargetView_, clearColor_);

	//深度バッファクリア
	pContext_->ClearDepthStencilView(pDepthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

//描画終了
HRESULT Direct3D::EndDraw()
{
	//スワップ（バックバッファを表に表示する）
	CHECK_HRESULT(pSwapChain_->Present(0, 0), "スワップに失敗しました");

	//成功
	return S_OK;
}

//解放処理
void Direct3D::Release()
{
	for (int shader = 0; shader < SHADER_MAX; shader++)
	{
		SAFE_RELEASE(shaderBundle_[shader].pRasterizerState_);
		SAFE_RELEASE(shaderBundle_[shader].pVertexLayout_);
		SAFE_RELEASE(shaderBundle_[shader].pPixelShader_);
		SAFE_RELEASE(shaderBundle_[shader].pVertexShader_);
	}
	SAFE_RELEASE(pDepthStencilView_);
	SAFE_RELEASE(pDepthStencil_);
	SAFE_RELEASE(pRenderTargetView_);
	SAFE_RELEASE(pSwapChain_);
	SAFE_RELEASE(pContext_);
	SAFE_RELEASE(pDevice_);
}

//画像描画時、Zバッファ無効
void Direct3D::SetTarget(bool isPict)
{
	//画像ではない
	if (isPict == false)
		pContext_->OMSetRenderTargets(1, &pRenderTargetView_, nullptr);					//描画先を設定

	//画像
	else
		pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView_);		//描画先を設定
}

//背景色の設定
void Direct3D::SetClearColor(float color[RGB])
{
	//色セット
	clearColor_[0] = color[0];
	clearColor_[1] = color[1];
	clearColor_[2] = color[2];
}

//背景色の設定
void Direct3D::SetClearColor(const float color[RGB])
{
	//色セット
	clearColor_[0] = color[0];
	clearColor_[1] = color[1];
	clearColor_[2] = color[2];
}

//背景色の取得
void Direct3D::GetClearColor(float color[RGB])
{
	//色セット
	color[0] = clearColor_[0];
	color[1] = clearColor_[1];
	color[2] = clearColor_[2];
}

//ウィンドウの中心を取得
int Direct3D::GetWindowCenterX()
{
	return screenWidth_ / 2;
}

//ウィンドウの中心を取得
int Direct3D::GetWindowCenterY()
{
	return screenHeight_ / 2;
}