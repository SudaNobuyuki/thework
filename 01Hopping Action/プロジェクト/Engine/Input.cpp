//インクルード
#include "Input.h"


//入力を管理する名前空間
namespace Input
{
	//ウィンドウハンドル
	HWND hwnd_;
	
	//DirectInputオブジェクト
	LPDIRECTINPUT8 pDInput_ = nullptr;		//入力用のバージョン8
	
	//キーボード
	LPDIRECTINPUTDEVICE8 pKeyDevice_ = nullptr;		//キーボードと直接やり取りするもの
	BYTE keyState_[256] = { 0 };					//キーボード記憶用
	BYTE prevkeyState_[256];						//前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice_ = nullptr;		//デバイスオブジェクト
	DIMOUSESTATE mouseState_;							//マウスの状態
	DIMOUSESTATE prevMouseState_;						//前フレームのマウスの状態
	XMVECTOR mousePosition_;							//マウスの位置記憶
	
	//初期化
	HRESULT Initialize(HWND hwnd)
	{
		//ウィンドウハンドル
		hwnd_ = hwnd;
		
		//DirectInputの本体の生成
		CHECK_HRESULT(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput_, nullptr), "DirectInputの本体の生成に失敗しました");
	
		//キーボード用の設定
		CHECK_HRESULT(pDInput_->CreateDevice(GUID_SysKeyboard, &pKeyDevice_, nullptr), "デバイスオブジェクトを作成に失敗しました");
		CHECK_HRESULT(pKeyDevice_->SetDataFormat(&c_dfDIKeyboard), "デバイスの種類の指定に失敗しました");
		pKeyDevice_->SetCooperativeLevel(hwnd_, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	
		//マウス
		CHECK_HRESULT(pDInput_->CreateDevice(GUID_SysMouse, &pMouseDevice_, nullptr), "デバイスオブジェクトを作成に失敗しました");
		CHECK_HRESULT(pMouseDevice_->SetDataFormat(&c_dfDIMouse), "デバイスの種類の指定に失敗しました");
		pMouseDevice_->SetCooperativeLevel(hwnd_, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
		
		//成功
		return S_OK;
	}

	//更新
	HRESULT Update()
	{
		//キーボード
		memcpy(prevkeyState_, keyState_, sizeof(prevkeyState_));														//全フレームのキー入力のコピー
		CHECK_HRESULT(pKeyDevice_->Acquire(), "キーボードの確保に失敗しました");										//キーボードの確保
		CHECK_HRESULT(pKeyDevice_->GetDeviceState(sizeof(keyState_), &keyState_), "キー入力の取得に失敗しました");		//キー入力チェック(配列に格納)

		//マウス
		memcpy(&prevMouseState_, &mouseState_, sizeof(mouseState_));			//全フレームのマウス入力のコピー
		pMouseDevice_->Acquire();												//マウスの確保(エラーは気にしない)
		pMouseDevice_->GetDeviceState(sizeof(mouseState_), &mouseState_);		//マウス入力チェック(配列に格納)(エラーは気にしない)
		
		//成功
		return S_OK;
	}

	//解放
	void Release()
	{
		SAFE_RELEASE(pMouseDevice_);
		SAFE_RELEASE(pKeyDevice_);
		SAFE_RELEASE(pDInput_);
	}

	/*****************
		キーボード
	*****************/
	//キーボードの入力チェック
	bool IsKey(int keyCode)
	{
		//入力チェック
		return keyState_[keyCode] & 0x80;
	}

	//キーボードの入力チェック(押した瞬間)
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		return IsKey(keyCode) & !(prevkeyState_[keyCode] & 0x80);
	}

	//2キーのどちらの入力されたかチェック(倍率あり)
	float KeyStateCalculation(int plus, int minus)
	{
		//計算
		return KeyStateCalculation(IsKey(plus), IsKey(minus), 1.0f);
	}

	//2キーのどちらの入力されたかチェック
	float KeyStateCalculation(int plus, int minus, float magnification)
	{
		//計算
		return (plus - minus) * magnification;
	}

	//パラメータパックが空になったら終了
	bool KeyStateCheck()
	{
		return false;
	}

	//パラメータパックが空になったら終了
	bool KeyStateCheckDown()
	{
		return false;
	}

	/*************
		マウス
	*************/
	//マウスの入力チェック
	bool IsMouseButton(int buttonCode)
	{
		//入力チェック
		return mouseState_.rgbButtons[buttonCode] & 0x80;
	}

	//マウスの入力チェック(押した瞬間)
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		return IsMouseButton(buttonCode) & !(prevMouseState_.rgbButtons[buttonCode] & 0x80);
	}

	//マウスの入力チェック(離した瞬間)
	bool IsMouseButtonUp(int buttonCode)
	{
		//今は押してなくて、前回は押している
		return !IsMouseButton(buttonCode) &  prevMouseState_.rgbButtons[buttonCode] & 0x80;
	}

	//ウィンドウ上のマウスカーソルの位置取得
	XMVECTOR GetMousePosition()
	{
		//現在の位置を取得
		return mousePosition_;
	}

	//マウスの位置を設定
	void SetMousePosition(int x, int y)
	{
		//ウィンドウプロシージャで更新
		mousePosition_ = XMVectorSet((float)x, (float)y, 0, 0);
	}

	//そのフレームでのマウスの移動量を取得(X)
	float GetMouseMoveX()
	{
		//移動量Xのみ取得
		return (float)mouseState_.lX;
	}

	//そのフレームでのマウスの移動量を取得(Y)
	float GetMouseMoveY()
	{
		//移動量Yのみ取得
		return (float)mouseState_.lY;
	}

	//そのフレームでのマウスの移動量を取得(ホイール)
	int GetMouseMoveWheel()
	{
		//移動量Zのみ取得
		return mouseState_.lZ;
	}

	//そのフレームでのマウスの移動量を取得(ALL)
	XMVECTOR GetMouseMove()
	{
		//移動量取得
		XMVECTOR result = XMVectorSet((float)mouseState_.lX, (float)mouseState_.lY, (float)mouseState_.lZ, 0);
		return result;
	}
}