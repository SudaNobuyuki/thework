//インクルード
#include "Sprite.h"
#include "Camera.h"


//コンストラクタ
Sprite::Sprite() :
	alpha_(1.0f), pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pConstantBuffer_(nullptr), pTexture_(nullptr), indexCount_(0)
{
}

//デストラクタ
Sprite::~Sprite()
{
}

//初期化
HRESULT Sprite::Initialize(std::string fileName)
{
	//頂点情報
	VERTEX  vertices[] =
	{
		{ XMVectorSet(-1.0f, 1.0f, 0.0f, 0.0f), XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f) },   //四角形の頂点（左上）
		{ XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f), XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f) },   //四角形の頂点（右上）
		{ XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f), XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f) },   //四角形の頂点（右下）
		{ XMVectorSet(-1.0f,-1.0f, 0.0f, 0.0f), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f) },   //四角形の頂点（左下）
	};

	//頂点データ用バッファの設定
	D3D11_BUFFER_DESC vertex;
	vertex.ByteWidth = sizeof(vertices);
	vertex.Usage = D3D11_USAGE_DEFAULT;
	vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex.CPUAccessFlags = 0;
	vertex.MiscFlags = 0;
	vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices;

	//頂点バッファの生成
	CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&vertex, &vertexData, &pVertexBuffer_), "頂点バッファの作成に失敗しました");

	//インデックス情報
	int index[] = { 0,2,3,  0,1,2 };

	//インデックスバッファを生成する
	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = sizeof(index);
	bufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;

	//インデックスバッファの作成
	D3D11_SUBRESOURCE_DATA subresourceData;
	subresourceData.pSysMem = index;
	subresourceData.SysMemPitch = 0;
	subresourceData.SysMemSlicePitch = 0;
	CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&bufferDesc, &subresourceData, &pIndexBuffer_), "インデックスバッファの作成に失敗しました");
	indexCount_ = sizeof(index) / sizeof(int);

	//コンスタントバッファ作成
	D3D11_BUFFER_DESC constant;
	constant.ByteWidth = sizeof(CONSTANT_BUFFER);
	constant.Usage = D3D11_USAGE_DYNAMIC;
	constant.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constant.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	constant.MiscFlags = 0;
	constant.StructureByteStride = 0;

	//コンスタントバッファの作成
	CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&constant, NULL, &pConstantBuffer_), "コンスタントバッファの作成に失敗しました");

	//テクスチャ準備
	pTexture_ = new Texture;
	CHECK_HRESULT(pTexture_->Load(fileName), "テクスチャの生成に失敗しました");

	//成功
	return S_OK;
}

//描画
HRESULT Sprite::Draw(Transform& transform)
{
	//2D用のシェーダ使用
	Direct3D::SetShaderBundle(SHADER_2D);

	//コンスタントバッファに渡す情報
	D3D11_MAPPED_SUBRESOURCE mapped;
	CONSTANT_BUFFER constant;
		
	//計算
	transform.Calclation();

	//画像サイズそのままの行列設定
	XMMATRIX adjustScale = XMMatrixScaling(
		(float)pTexture_->GetWidth(),
		(float)pTexture_->GetHeight(), 1);

	//1×1ピクセルに調整する行列
	XMMATRIX miniScale = XMMatrixScaling(
		1.0f / Direct3D::screenWidth_,
		1.0f / Direct3D::screenHeight_, 1);

	//コンスタントバッファに渡す情報
	constant.worldMatrix = XMMatrixTranspose(adjustScale
		* transform.scaleMatrix_ * transform.rotationMatrix_
		* miniScale * transform.movingMatrix_);

	//アルファ値
	constant.alpha = alpha_;

	//GPUからのデータアクセスを止める
	CHECK_HRESULT(Direct3D::pContext_->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped), "GPUからのアクセスを止めることに失敗しました");
	memcpy_s(mapped.pData, mapped.RowPitch, (void*)(&constant), sizeof(constant));		//データを値を送る

	//テクスチャとサンプラーをシェーダーへ
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();
	Direct3D::pContext_->PSSetSamplers(0, 1, &pSampler);
	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();
	Direct3D::pContext_->PSSetShaderResources(0, 1, &pSRV);
	Direct3D::pContext_->Unmap(pConstantBuffer_, 0);

	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext_->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	//インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext_->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext_->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
	Direct3D::pContext_->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

	//頂点個数
	Direct3D::pContext_->DrawIndexed(indexCount_, 0, 0);

	//成功
	return S_OK;
}

//解放
void Sprite::Release()
{
	pTexture_->Release();
	pConstantBuffer_->Release();
	pIndexBuffer_->Release();
	pVertexBuffer_->Release();
}