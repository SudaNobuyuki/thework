//インクルード
#include "Stage.h"
#include "PlayScene.h"
#include "StageConfig.h"
#include "Engine/CsvReader.h"
#include "Engine/Input.h"
#include "Engine/Model.h"


//定数
//BackgroundModelRotationProcess()
static const float BACKGROUND_MODEL_ROTATION_SPEED = 0.1f;		//背景モデルを回転させる速さ

//StageModelDrawProcess()
static const int START_HEIGHT = 1;		//モデル描画の開始位置


//マクロ
//StageModelDrawProcess()
#define IS_DRAW (y == START_HEIGHT || ( x != 0 && x != stageWidth_ - 1 && z != 0 && z != stageDepth_ - 1 && stageData_[y][x][z] != FLOOR))		//Y座標が1またはx,zがサイドではなく描画対象が床ではない場合描画する処理

//StageInformationInitialSetting()
#define GET_HEIGHT (stageConfig.GetValue(0, 0))		//高さ取得
#define GET_WIDTH  (stageConfig.GetValue(1, 0))		//幅取得
#define GET_DEPTH  (stageConfig.GetValue(2, 0))		//奥行取得


//コンストラクタ
Stage::Stage(GameObject * parent) : GameObject(parent, "Stage"),
	backgroundModelID_(INITIAL_ID),
	stageHeight_(0), stageWidth_(0), stageDepth_(0)
{
	//各ステージモデルの初期化
	for (int i = 0; i < STAGE_MODEL_MAXIMUM; i++)
		stageModelID_[i] = INITIAL_ID;
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//各ステージのモデルの初期設定
	StageModelInitialSetting();

	//背景モデルの初期設定
	BackgroundModelInitialSetting();

	//ステージ情報の初期設定
	StageDataInitialSetting();
}

//更新
void Stage::Update()
{
	//背景モデルを回転させる処理
	BackgroundModelRotationProcess();
}

//描画
void Stage::Draw()
{
	//各ステージのモデルを描画させる処理
	StageModelDrawProcess();

	//背景の描画
	Model::Draw(backgroundModelID_);
}

//開放
void Stage::Release()
{
}

//各ステージのモデルの初期設定
void Stage::StageModelInitialSetting()
{
	//各ステージモデルデータのパス
	std::string stageModelPath[STAGE_MODEL_MAXIMUM] =
	{
		"Assets/PlayScene/Floor.fbx",
		"Assets/PlayScene/Wall.fbx",
		"Assets/PlayScene/Goal.fbx",
	};

	//各ステージモデルデータのロード
	for (int i = 0; i < STAGE_MODEL_MAXIMUM; i++)
	{
		//ステージモデルデータのロード
		stageModelID_[i] = Model::Load(stageModelPath[i]);
		assert(stageModelID_[i] >= 0);
	}
}

//各ステージのモデルを描画させる処理
void Stage::StageModelDrawProcess()
{
	//ステージ内全座標
	for (int y = START_HEIGHT; y < stageHeight_; y++)
	{
		for (int x = 0; x < stageWidth_; x++)
		{
			for (int z = 0; z < stageDepth_; z++)
			{
				//生成位置調整
				transform_.position_ = XMVectorSet((float)x, (float)y, (float)z, 0);
				Model::SetTransform(stageModelID_[stageData_[y][x][z]], transform_);

				//Y座標が1またはx,zがサイドではなく描画対象が床ではない場合描画する処理
				if (IS_DRAW)
					Model::Draw(stageModelID_[stageData_[y][x][z]]);
			}
		}
	}
}

//背景モデルの初期設定
void Stage::BackgroundModelInitialSetting()
{
	//背景モデルデータのロード
	backgroundModelID_ = Model::Load("Assets/PlayScene/Sky.fbx");
	assert(backgroundModelID_ >= 0);
}

//背景モデルを回転させる処理
void Stage::BackgroundModelRotationProcess()
{
	//ポーズ中でなくゴールしていない場合の処理
	if (!dynamic_cast<PlayScene*>(pParent_)->IsPauseOrGoal())
	{
		//背景モデルを回転
		backgroundModelTransform_.rotate_.vecY += BACKGROUND_MODEL_ROTATION_SPEED;

		//背景モデルの角度を設定
		Model::SetTransform(backgroundModelID_, backgroundModelTransform_);
	}
}

//ステージ情報の初期設定
void Stage::StageDataInitialSetting()
{
	//ステージ及びステージ設定の読み込み
	CsvReader stage;
	stage.Load(StageConfig::GetStageDataPath());
	CsvReader stageConfig;
	stageConfig.Load(StageConfig::GetStageDataConfigPath());

	//フィールドの縦横高さ取得読み込み
	stageHeight_ = GET_HEIGHT;		//高さ
	stageWidth_ = GET_WIDTH;		//幅
	stageDepth_ = GET_DEPTH;		//奥行

	//ステージ情報の初期化
	ZeroMemory(stageData_, sizeof(stageData_));

	//ステージ情報を読み取る
	for (int y = 0; y < stageHeight_; y++)
	{
		for (int x = 0; x < stageWidth_; x++)
		{
			for (int z = 0; z < stageDepth_; z++)
			{
				stageData_[y][x][z] = stage.GetValue(x, z + y * stageDepth_);
			}
		}
	}
}