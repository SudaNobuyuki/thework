//インクルード
#include "StageConfig.h"


//定数
static const std::string stagePath_[StageConfig::STAGE_MAXIMUM]				//ステージのパス
{
	"Assets/StageData/Stage1/Stage1",
	"Assets/StageData/Stage2/Stage2",
	"Assets/StageData/Stage3/Stage3",
};
static const std::string STAGE_IMAGE = ".png";								//ステージ画像
static const std::string STAGE_DESCRIPTION_IMAGE = "Description.png";		//ステージ説明画像
static const std::string STAGE_DATA = ".csv";								//ステージデータ
static const std::string STAGE_DATA_CONFIG = "_Config.csv";					//ステージデータ設定
static const std::string STAGE_RECORD = ".txt";								//ステージ記録


//ステージコンフィグを管理する名前空間
namespace StageConfig
{
	//現在選択しているステージ
	STAGES selectStage_ = STAGE_1;

	//ステージ画像のパスの取得
	std::string GetStageImagePath(int stage)
	{
		//ステージ画像のパスを返す
		return stagePath_[stage] + STAGE_IMAGE;
	}

	//ステージ説明画像のパスの取得
	std::string GetStageDescriptionImagePath(int stage)
	{
		//ステージ説明画像のパスを返す
		return stagePath_[stage] + STAGE_DESCRIPTION_IMAGE;
	}

	//ステージデータのパスの取得
	std::string GetStageDataPath()
	{
		//ステージデータのパスを返す
		return stagePath_[selectStage_] + STAGE_DATA;
	}

	//ステージデータ設定のパスの取得
	std::string GetStageDataConfigPath()
	{
		//ステージデータ設定のパスを返す
		return stagePath_[selectStage_] + STAGE_DATA_CONFIG;
	}

	//ステージ記録のパスの取得
	std::string GetStageRecordPath()
	{
		//ステージ記録のパスを返す
		return stagePath_[selectStage_] + STAGE_RECORD;
	}
}