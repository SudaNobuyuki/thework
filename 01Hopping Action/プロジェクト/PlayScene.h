//インクルードガード
#pragma once
#ifndef PLAY_SCENE_H
#define PLAY_SCENE_H
#endif // !PLAY_SCENE_H


//インクルード
#include "Engine/GameObject.h"
#include "Engine/SceneManager.h"


//列挙
//プレイシーンの状態を列挙
enum SCENE_STATE
{
	BEFORE_THE_GAME,		//ゲーム開始前
	IN_GAME,				//ゲーム中
	AFTER_THE_GAME,			//ゲーム終了
};


//プレイシーンを管理するクラス
class PlayScene : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	PlayScene(GameObject* parent);

	//デストラクタ
	//param :なし
	~PlayScene();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

public:
	/*自身を親とする各オブジェクトのポインタ*/

	//ステージオブジェクトポインタ
	GameObject* pStage_;

	//プレイヤーオブジェクトポインタ
	GameObject* pPlayer_;

	//タイマーオブジェクトポインタ
	GameObject* pTimer_;

	//ポーズオブジェクトポインタ
	GameObject* pPause_;

	//リザルトオブジェクトポインタ
	GameObject* pResult_;

	//各オブジェクトのポインタの初期化(解放と共通)
	//param :なし
	//return:なし
	void ObjectsInitialize() { ObjectsRelease(); };

	//各オブジェクトのポインタの解放
	//param :なし
	//return:なし
	void ObjectsRelease();

public:
	/*自身を親とする各オブジェクトの生成関数*/

	//ステージオブジェクトの生成
	//param :なし
	//return:なし
	void StageGeneration();

	//プレイヤーオブジェクトの生成
	//param :なし
	//return:なし
	void PlayerGeneration();

	//タイマーオブジェクトの生成
	//param :なし
	//return:なし
	void TimerGeneration();

	//ポーズオブジェクトの生成
	//param :なし
	//return:なし
	void PauseGeneration();

	//リザルトオブジェクトの生成
	//param :なし
	//return:なし
	void ResultGeneration();

public:
	/*自身を親とする特定オブジェクトを殺す関数*/

	//プレイヤーオブジェクトを殺す
	//param :なし
	//return:なし
	void KillPlayer() { pPlayer_->KillMe(); }

	//ポーズオブジェクトを殺す
	//param :なし
	//return:なし
	void KillPause() { pPause_->KillMe(); }

private:
	/*プレイシーン全体でポーズ中かやゴールしたかを記憶*/

	//ポーズ中か
	bool isPause_;

	//ゴールしたか
	bool isGoal_;

public:
	/*ポーズ中かの設定*/

	//ポーズ中かの設定
	//param :ポーズ中か
	//return:なし
	void SetIsPause(bool isPause) { isPause_ = isPause; }

public:
	/*ポーズ中かやゴールしたかを確認*/

	//ポーズ中かを確認
	//param :なし
	//return:ポーズ中か
	bool IsPause() { return isPause_; }

	//ゴールしたかを確認
	//param :なし
	//return:ゴールしたか
	bool IsGoal() { return isGoal_; }

	//ポーズ中かゴールしているかを確認
	//param :なし
	//return:いずれかをしているか
	bool IsPauseOrGoal() { return IsPause() | IsGoal(); }

public:
	/*NewRecordか*/

	//NewRecordか
	//param :なし
	//return:過去最高記録より今回の記録のほうが上回っているか
	bool IsNewRecord();

public:
	/*ステージの情報取得*/

	//そこは壁か調べる
	//param :座標
	//return:壁ならTrue、床ならFalse
	bool IsWall(XMVECTOR& position);

	//そこがゴールか調べる(ゴール時にはゴールしたと記憶)
	//param :座標
	//return:ゴールならTrue、床ならFalse
	bool IsGoal(XMVECTOR& position);

public:
	/*シーンを変更*/

	//シーンマネージャーオブジェクトに自身のシーンへの切り替えを要請
	//param :なし
	//return:なし
	void ReScene() { dynamic_cast<SceneManager*>(pParent_)->ReScene(); }

	//シーンマネージャーオブジェクトにステージセレクトシーンへの切り替えを要請
	//param :なし
	//return:なし
	void ChangeScene() { dynamic_cast<SceneManager*>(pParent_)->ChangeScene(SCENE_ID_SELECT); }
};