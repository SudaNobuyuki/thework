//インクルードガード
#pragma once
#ifndef TITLE_H
#define TITLE_H
#endif // !TITLE_H


//インクルード
#include "Engine/GameObject.h"


//タイトル画像を管理するクラス
class Title : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Title(GameObject* parent);

	//デストラクタ
	//param :なし
	~Title();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*キー入力を促す画像("PressEnterToStart")管理*/

	//"PressEnterToStart"画像ID(pressEnter)
	int pressEnterImageID_;

	//"PressEnterToStart"画像の累計描画時間(0〜60)
	int pressEnterImageTotalDrawingTime_;

	//"PressEnterToStart"画像のα値
	float pressEnterImageAlpha_;

	//"PressEnterToStart"画像の初期設定
	//param :なし
	//return:なし
	void PressEnterImageInitialSetting();

	//"PressEnterToStart"画像のα値設定する処理
	//param :なし
	//return:なし
	void PressEnterImageProcessSettingAlpha();

private:
	/*タイトル画像管理*/

	//タイトル画像ID
	int titleImageID_;

	//タイトル画像の初期設定
	//param :なし
	//return:なし
	void TitleImageInitialSetting();
};