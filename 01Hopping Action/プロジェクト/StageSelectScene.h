//インクルードガード
#pragma once
#ifndef SELECT_SCENE_H
#define SELECT_SCENE_H
#endif // !SELECT_SCENE_H


//インクルード
#include "Engine/GameObject.h"
#include "Engine/SceneManager.h"


//ステージセレクトシーンを管理するクラス
class StageSelectScene : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	StageSelectScene(GameObject* parent);

	//デストラクタ
	//param :なし
	~StageSelectScene();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*自身を親とする各オブジェクトのポインタ*/

	//ステージセレクトオブジェクトポインタ
	GameObject* pSelector;

	//各オブジェクトのポインタの初期化(解放と共通)
	//param :なし
	//return:なし
	void ObjectsInitialize() { ObjectsRelease(); };

	//各オブジェクトのポインタの解放
	//param :なし
	//return:なし
	void ObjectsRelease();

public:
	/*自身を親とする各オブジェクトの生成関数*/

	//ステージセレクトオブジェクトの生成
	//param :なし
	//return:なし
	void SelectorGeneration();

public:
	/*シーンを変更*/

	//シーンマネージャーオブジェクトにシーン切り替えを要請
	//param :なし
	//return:なし
	void NextScene() { dynamic_cast<SceneManager*>(pParent_)->NextScene(); }
};