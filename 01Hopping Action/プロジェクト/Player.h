//インクルードガード
#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#endif // !PLAYER_H


//インクルード
#include "Engine/GameObject.h"


//操作キャラを管理するクラス
class Player : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Player(GameObject* parent);

	//デストラクタ
	//param :なし
	~Player();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*中央線および開始カウント画像管理*/

	//中央に表示される画像の種類
	enum CENTER_IMAGES
	{
		CENTER_LINE,
		GO,
		ONE,
		TWO,
		THREE,
		CENTER_IMAGE_MAXIMUM
	};

	//中央に表示される画像ID
	int centerImageID_[CENTER_IMAGE_MAXIMUM];

	//現在中央に表示している画像
	int centerImage_;

	//中央に表示する画像を変更する周期(1秒(60フレーム)毎にTHREE〜CENTER_LINEまでを遷移)
	int centerImageChangeCycle_;

	//各中央に表示する画像の初期設定
	//param :なし
	//return:なし
	void centerImageInitialSetting();

	//中央に表示する画像を設定する処理
	//param :なし
	//return:なし
	void CenterImageSettingProcess();

private:
	/*プレイヤーの操作に関わる各種変数・処理*/

	//ベクトルを回転させる際に使用する行列
	XMMATRIX playerAngleYMatrix_;

	//プレイヤーの初期設定
	//param :なし
	//return:なし
	void PlayerInitialSetting();

	//プレイヤーがゴールしたか
	//param :なし
	//return:なし
	void IsGoal();

private:
	/*プレイヤーの操作に関わる各種オプション*/

	//ポーズ中か(ポーズオブジェクトと同期させる)
	bool isPause_;

	//中央の画像の描画およびそのカウントが有効かどうか
	bool isCentralImageDrawingAndItsCount_;

	//プレイヤーの移動が有効かどうか
	bool isPlayerMove_;

	//プレイヤーの移動速度の状態
	enum PLAYER_MOVE_STATE
	{
		NORMAL,
		DASH,
		SLOWRY
	}playerMoveState_;

	//マウスカーソルが表示されているか
	int isMouseCursorDisplayed_;

	//マウス操作(カメラ操作)が有効かどうか
	bool isMouseControl_;

	//一度のみプレイヤーの操作(キーボード)各種設定を有効にするための有効にしたかを記憶
	bool isPlayerOperationRelateOptionOnceActivation_;

	//ポーズ(動作を終了)時の処理
	//param :なし
	//return:なし
	void PauseProcess();

private:
	/*プレイヤーの操作(キーボード)に関わる処理*/

	//入力による移動量
	XMVECTOR movingDistanceByEntry_;

	//プレイヤーの移動量
	XMVECTOR playerMovingDistance_;

	//プレイヤーが空中にいるか
	bool isPlayerInTheAir_;

	//プレイヤーのジャンプ量
	float playerJumpAmount_;

	//一度のみプレイヤーの操作(キーボード)各種設定を有効にする処理
	//param :なし
	//return:なし
	void PlayerOperationRelateOptionOnceActivateProcess();

	//プレイヤーの移動の処理
	//param :なし
	//return:なし
	void PlayerMoveProcess();

	//プレイヤーのY軸移動(ジャンプ)の処理
	//param :なし
	//return:なし
	void PlayerJumpProcess();

	//プレイヤーのY軸方向の移動量による壁へのめり込みを回避させる処理
	//param :なし
	//return:なし
	void ReturnFromTheWallOnTheYAxis();

	//プレイヤーのXZ軸方向の移動量による壁へのめり込みを回避させる処理
	//param :x,z軸のどれか
	//return:なし
	void ReturnFromTheWallOnTheXorZAxis(int axis);

	//プレイヤーの移動速度の設定処理
	//param :なし
	//return:なし
	void PlayerMoveVelocitySettingProcess();

private:
	/*プレイヤーの操作(マウス)に関わる処理*/

	//カメラの高さ
	float cameraHeight_;

	//マウスの操作設定の処理
	//param :なし
	//return:なし
	void MouseControlProcess();

	//カメラの移動処理
	//param :なし
	//return:なし
	void CameraMoveProcess();
};