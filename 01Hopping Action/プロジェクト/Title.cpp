//インクルード
#include "Title.h"
#include "StartScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//定数
//Initialize()
static const float CLEAR_COLOR[RGB] = { 1, 1, 1 };		//背景色を白

//PressEnterImageProcessSettingAlpha()
static const int PRESS_ENTER_IMAGE_MAX_DRAWING_TIME = 60;		//pressEnterImageDrawTime_の最大値


//マクロ
//PressEnterImageSetAlphaProgress()
#define DECIDE_PRESS_ENTER_IMAGE_ALPHA (abs(sin(XMConvertToRadians(pressEnterImageTotalDrawingTime_ * 3.0f))))		//"PressEnterToStart"画像のα値を決める


//コンストラクタ
Title::Title(GameObject * parent) : GameObject(parent, "Title"),
	pressEnterImageID_(INITIAL_ID), pressEnterImageTotalDrawingTime_(0), pressEnterImageAlpha_(0),
	titleImageID_(INITIAL_ID)
{
}

//デストラクタ
Title::~Title()
{
}

//初期化
void Title::Initialize()
{
	//背景色変更
	Direct3D::SetClearColor(CLEAR_COLOR);

	//"PressEnterToStart"画像の初期設定
	PressEnterImageInitialSetting();

	//タイトル画像の初期設定
	TitleImageInitialSetting();
}

//更新
void Title::Update()
{
	//"PressEnterToStart"画像のα値設定する処理
	PressEnterImageProcessSettingAlpha();

	//ステージセレクトシーンへ遷移する処理(いずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合)
	if (Input::KeyStateCheckDown(DIK_SPACE, DIK_RETURN) || Input::IsMouseButtonDown(MOUSE_LEFT))
		dynamic_cast<StartScene*>(pParent_)->NextScene();
}

//描画
void Title::Draw()
{
	//"PressEnterToStart"画像の描画
	Image::Draw(pressEnterImageID_);

	//タイトル画像の描画
	Image::Draw(titleImageID_ );
}

//開放
void Title::Release()
{
}

//"PressEnterToStart"画像の初期設定
void Title::PressEnterImageInitialSetting()
{
	//"PressEnterToStart"画像データのロード
	pressEnterImageID_ = Image::Load("Assets/StartScene/PressEnterToStart.png");
	assert(pressEnterImageID_ >= 0);

	//タイトル画像の拡大率をウィンドウサイズに調整
	Image::SetWindowSize(pressEnterImageID_);

	//"PressEnterToStart"画像のトランスフォームを設定
	Image::SetTransform(pressEnterImageID_, transform_);
}

//"PressEnterToStart"画像のα値設定する処理
void Title::PressEnterImageProcessSettingAlpha()
{
	//pressEnterImageDrawTime_をインクリメントした際に60を超えた場合に0に戻す処理
	if (++pressEnterImageTotalDrawingTime_ >= PRESS_ENTER_IMAGE_MAX_DRAWING_TIME)
		pressEnterImageTotalDrawingTime_ = 0;

	//"PressEnterToStart"画像のα値を決める
	pressEnterImageAlpha_ = DECIDE_PRESS_ENTER_IMAGE_ALPHA;

	//"PressEnterToStart"画像のα値を渡す
	Image::SetAlpha(pressEnterImageID_, pressEnterImageAlpha_);
}

//タイトル画像の初期設定
void Title::TitleImageInitialSetting()
{
	//タイトル画像データのロード
	titleImageID_ = Image::Load("Assets/StartScene/Title.png");
	assert(titleImageID_ >= 0);

	//タイトル画像の拡大率をウィンドウサイズに調整
	Image::SetWindowSize(titleImageID_);
}