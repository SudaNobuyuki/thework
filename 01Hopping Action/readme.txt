■開発者情報
氏名：須田信行
連絡先：190309@jc-21.jp

■作品概要
3D空間内でより早くゴールを目指すゲームです

■動作環境
Windows10
キーボードとマウスを使用

■起動方法
実行ファイルフォルダ内のHopping Action.exeを起動してください。

■操作方法
このゲームは以下のシーンで構成されています。
1.タイトルシーン
2.ステージセレクトシーン
3.プレイシーン
4.リザルトシーン
----------------------------------
1.タイトルシーンの操作
・Press Enter To Startが表示された際にエンターキーを押すことでステージセレクトシーンへ切り替わります。

2.ステージセレクトシーンの操作
・選択されるステージは左右キーの入力で切り替わります。
・始めたいステージが決まった際にエンターキーを押すことでそのステージのプレイシーンへ切り替わります。

3.プレイシーンの操作
・初めにタイマーが表示され、GOの表示とともに行動を開始できます。
・WASDでプレイヤーを移動できます。
・マウスを動かすことで視点が動きます。
・左CTRLを押しながらWを押すことでダッシュできます。
・左SHIFTを押すことでゆっくり移動します。ただし段差などからは普通に落ちます。
・スペースキーでジャンプをできます。
・白黒タイルのブロックがゴールです。このブロックを踏むことでリザルトシーンへ切り替わります。

4.リザルトシーンの操作
・タイムの表示が終わった後にエンターキーを押すことでステージセレクトシーンへ切り変わります。

----------------------------------

■アピールポイント
・プレイヤーの挙動全般
・壁との当たり判定方法

■開発期間
8か月(ただし、6か月間はDirectXの学習、およびゲームエンジンの制作。ゲームメイン部分の作成は1か月程度)

■開発環境・言語
・VisualStudio
・DirectX11
・Bitbucket(SourceTree)
・Maya
・Photoshop
・C++
・HLSL

■謝辞
以下のフリー素材を使用しています。
・モデルテクスチャ
	(TEXTURE HAVEN)https://texturehaven.com/

■ファイル内容(名前順)
プロジェクト
│Engine						授業で作成
│├Camera(cpp,h)				ゲーム内のカメラを管理
│├CsvReader(cpp,h)			.csvファイルの読み取りを管理
│├Direct3D(cpp,h)				DirectX全般の設定を管理
│├Fbx(cpp,h)					.fbxモデルを管理
│├GameObject(cpp,h)			ゲーム内のオブジェクトのスーパークラス
│├Image(cpp,h)				画像を管理
│├Input(cpp,h)				入力を管理
│├Model(cpp,h)				Fbx(cpp,h)を管理
│├SceneManager(cpp,h)			シーンを管理
│├Sprite(cpp,h)				Image(cpp,h)を管理
│├Texture(cpp,h)				モデルのテクスチャを管理
│└Transform(cpp,h)			ゲーム内空間を管理
│
├Main							WinMain
├Pause(cpp,h)					プレイシーンでのポーズの処理を管理
├Player(cpp,h)					プレイシーンでのプレイヤーの挙動を管理
├PlayScene(cpp,h)				プレイシーンを管理
├Result(cpp,h)					プレイシーンでのリザルト処理を管理
├Selector(cpp,h)				ステージセレクトシーンでのステージセレクトを管理
├Splash(cpp,h)					タイトルシーンでのスプラッシュ画像を管理
├Stage(cpp,h)					プレイシーンでのステージを管理
├StageConfig(cpp,h)			ステージ情報を管理
├StageSelectScene(cpp,h)		ステージセレクトシーンを管理
├StartScene(cpp,h)				スタートシーンを管理
├Timer(cpp,h)					プレイシーンでの時間を管理
└Title(cpp,h)					スタートシーンでのタイトル画像を管理

実行ファイルフォルダ
├Assets
│├Numbers
││├0.png						0の画像
││├1.png						1の画像
││├2.png						2の画像
││├3.png						3の画像
││├4.png						4の画像
││├5.png						5の画像
││├6.png						6の画像
││├7.png						7の画像
││├8.png						8の画像
││├9.png						9の画像
││├Go.png					ステージ開始時の画像
││├分.png					分の画像
││└秒.png					秒の画像
││
│├PlayScene
││├Center.png				プレイシーンの中央の十字の画像
││├Floor.fbx					床モデル
││├Goal.fbx					ゴールモデル
││├Goal.png					ゴールモデルテクスチャ
││├mar.jpg					床モデルテクスチャ
││├Pause.png					ポーズ時の背景画像
││├Reselect.png				ポーズ時のステージ選択へ戻る画像
││├Restart.png				ポーズ時の再スタートの画像
││├Return.png				ポーズ時のゲームに戻る画像
││├roof.jpg					ブロックモデルテクスチャ
││├Sky.png					空モデルテクスチャ
││├Sky.fbx					空モデル
││└Wall.fbx					ブロックモデル
││
│├Result
││├NewRecord.png				"NewRecord"の画像
││├Record.png				"過去最高記録"の画像
││├Result.jpg				背景画像
││└Time.png					"今回の記録"の画像
││
│├SelectScene
││├Flame.png					説明枠の画像
││├Map1.png					Map1のセレクト画像
││├Map1Mes.png				Map1の説明
││├Map2.png					Map2のセレクト画像
││├Map2Mes.png				Map2の説明
││├Map3.png					Map3のセレクト画像
││└Map3Mes.png				Map3の説明
││
│├Shader
││├Simple2D.hlsl				画像用シェーダ
││└Simple3D.hlsl				モデル用シェーダ、diffuseのみ使用
││
│├StageData
││├Stage1
│││├Stage1.csv				ステージcsvデータ
│││├Stage1.png				ステージ画像
│││├Stage1.txt				ステージのタイム
│││├Stage1.xlsx				ステージの元データ
│││├Stage1_Config.csv		ステージの設定
│││└Stage1Description.csv	ステージの説明
│││
││├Stage2
│││├Stage2.csv				ステージcsvデータ
│││├Stage2.png				ステージ画像
│││├Stage2.txt				ステージのタイム
│││├Stage2.xlsx				ステージの元データ
│││├Stage2_Config.csv		ステージの設定
│││└Stage2Description.csv	ステージの説明
│││
││└Stage3
││　├Stage3.csv				ステージcsvデータ
││　├Stage3.png				ステージ画像
││　├Stage3.txt				ステージのタイム
││　├Stage3.xlsx				ステージの元データ
││　├Stage3_Config.csv		ステージの設定
││　└Stage3Description.csv	ステージの説明
││
│└StartScene
│　├Splash.png				スプラッシュ画像
│　├PressEnterToStart.png		"PressEnterToStart"画像
│　└title.png					タイトル画像
│
└Hopping Action.exe

Hopping Action.mp4				もしゲームが出来なかった際にこちらで成果物を見てください。

readme.txt